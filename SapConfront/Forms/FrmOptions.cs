﻿using System;
using System.Windows.Forms;

namespace SapConfront
{
    public partial class FrmOptions : Form
    {
        public FrmOptions(AnagraficaSapContext db)
        {
            DB = db ?? throw new ArgumentNullException(nameof(db));
            Options = AnagraficaSapOptions.Read(DB);

            InitializeComponent();
        }
        public AnagraficaSapContext DB { get; }
        public AnagraficaSapOptions Options { get; }

        private void addUlBtn_Click(object sender, EventArgs e)
        {
           if (textULIgnore.Text == "")
            {
                MessageBox.Show("Inserire l'UL da Aggiungere alla Lista" ,"Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textULIgnore.Focus();
                return;
            }
           if (ignoreUlList.FindString(textULIgnore.Text)>-1)
            {
                MessageBox.Show("Elemento già Esistente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textULIgnore.Text = "";
                textULIgnore.Focus();
                return;
            }
            ignoreUlList.Items.Add(textULIgnore.Text);
            textULIgnore.Text = "";
            textULIgnore.Focus();
        }

        private void listUL_DoubleClick(object sender, EventArgs e)
         {
            textULIgnore.Text = ignoreUlList.SelectedItem.ToString();
         }

        private void removeUlBtn_Click(object sender, EventArgs e)
         {
            if (ignoreUlList.SelectedIndex==-1)
             {
                MessageBox.Show("Selezionare l'UL da Rimuovere", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textULIgnore.Focus();
                return;
             }
            ignoreUlList.Items.Remove(ignoreUlList.SelectedItem);
            textULIgnore.Text = "";
         }

        private void saveOptionsBtn_Click(object sender, EventArgs e)
        {
            int.TryParse(combomese.Text, out int periodoScaricoMese);
            int.TryParse(comboanno.Text, out int periodoScaricoAnno);

            var anagraficaSapOptions = new AnagraficaSapOptions
            {
                PeriodoScaricoMese = periodoScaricoMese,
                PeriodoScaricoAnno = periodoScaricoAnno
            };

            foreach (var item in ignoreUlList.Items)
            {
                anagraficaSapOptions.IgnoraUlInZRE13.Add(item.ToString());
            }

            AnagraficaSapOptions.Save(DB, anagraficaSapOptions);

            Close();
        }

        private System.Drawing.Color NormalColor = System.Drawing.Color.White;
        private System.Drawing.Color FocusColor = System.Drawing.Color.Yellow;
        private void EnterEvent(object sender, EventArgs e)
        {
            if (sender is TextBox)
                ((TextBox)sender).BackColor = FocusColor;
        }

        private void LeaveEvent(object sender, EventArgs e)
        {
            if (sender is TextBox)
                ((TextBox)sender).BackColor = NormalColor;
        }

        private void FrmOptions_Load(object sender, EventArgs e)
        {
            foreach (var ul in Options.IgnoraUlInZRE13)
            {
               ignoreUlList.Items.Add(ul);
            }

            combomese.Text = $"{Options.PeriodoScaricoMese}".PadLeft(2, '0');
            comboanno.Text = $"{Options.PeriodoScaricoAnno}";

            textULIgnore.Enter += EnterEvent;
            textULIgnore.Leave += LeaveEvent;

            var toolStrip = new ToolTip();
            toolStrip.ToolTipIcon = ToolTipIcon.Info;
            toolStrip.ToolTipTitle = "Guida";
            toolStrip.AutoPopDelay = 10000;
            toolStrip.IsBalloon = true;
            toolStrip.ShowAlways = true;

            toolStrip.SetToolTip(ignoreUlList, "-Aggiungere UL:Inerire in casella testo e premere '+'" + "\r\n" + "-Rimuovere UL:Selezionare dalla Lista l'UL e premere '-'");
            toolStrip.SetToolTip(textULIgnore, "Inserire l'UL da Ignorare");
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
      
        private void combomese_Validated(object sender, EventArgs e)
        {
            int combomeseparse;
            if (combomese.Text=="" || int.TryParse(combomese.Text,out combomeseparse) == false)
            {
                MessageBox.Show("Mese non valorizzato o Formato Mese errato", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                combomese.Focus();
                return;
            }
            else
            if (int.Parse(combomese.Text) < 1 || int.Parse(combomese.Text) > 12) 
            {
                MessageBox.Show("Mese non Corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                combomese.Focus();
                return;
            }
            combomese.Text = combomese.Text.PadLeft(2, '0');
        }

        private void comboanno_Validated(object sender, EventArgs e)
        {
            //DateTime dDate;
            //if (DateTime.TryParse(comboanno.Text, out dDate) == false)
            //{
            //    MessageBox.Show("Anno non Corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    comboanno.Focus();
            //    return;
            //}

            int year;
            if (int.TryParse(comboanno.Text, out year)==false || (year < 1942 || year > 2142))
            {
                MessageBox.Show("Anno non Corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                comboanno.Focus();
                return;
            }
         



            if (comboanno.Text == "")
            {
                MessageBox.Show("Anno non Corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                comboanno.Focus();
                return;
            }
        }

        private void FrmOptions_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                if (this.ActiveControl.GetType()== typeof(TextBox) || this.ActiveControl.GetType() == typeof(ComboBox))
                {
                    e.SuppressKeyPress = true;
                    this.ProcessTabKey(true);
                }                                  
            }
        }      
    }
}
