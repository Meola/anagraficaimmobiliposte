﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Windows.Forms;


namespace SapConfront
{
    public partial class Frm_SelectFile : Form
    {
        public string refbuildingFilename;
        public string sapbuildingFilename;
        public string sapregolaFilename;
        public string sapstatoFilename;
        public string sapstatoUeFilename;
        private DataSet refBuildingDataSet;
        private DataSet sapreDataSet;
        private DataSet regoleSapDataSet;
        private DataSet statoEdfDataSet;
        private DataSet statoUeDataSet;

        public Frm_SelectFile(AnagraficaSapContext db)
        {
            InitializeComponent();

            DB = db ?? throw new ArgumentNullException(nameof(db));
            Options = AnagraficaSapOptions.Read(db);

            refBuildingDataSet = new DataSet();
            sapreDataSet = new DataSet();
        }

        public AnagraficaSapContext DB { get; private set; }
        public AnagraficaSapOptions Options { get; set; }

        private void refFileBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((openFileDialog1.FileName) != "")
                {
                    refbuildingFilename = openFileDialog1.FileName;
                    Lbl_Ref.Text = Path.GetFileName(refbuildingFilename);
                    importBuildingBtn.Enabled = File.Exists(refbuildingFilename) &&
                                          File.Exists(sapbuildingFilename);
                }
            }
        }

        private void esciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }

        private void sapFileBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((openFileDialog1.FileName) != "")
                {
                    sapbuildingFilename = openFileDialog1.FileName;
                    Lbl_Sap.Text = Path.GetFileName(sapbuildingFilename);
                    importBuildingBtn.Enabled = File.Exists(refbuildingFilename) &&
                                          File.Exists(sapbuildingFilename);
                }
            }
        }

        private void importBuildingBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists(refbuildingFilename) &&
                File.Exists(sapbuildingFilename))
            {
                importBuildingBtn.Enabled = false;
                compareBuildingsBtn.Enabled = false;
                refFileBtn.Enabled = false;
                sapFileBtn.Enabled = false;
                importBuildingPBar.Value = 0;
                importBuildingPBar.Visible = true;
                importRegolaBtn.Enabled = true;

                importBuildingWorker.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show(this, "Specificare entrambi i file Ref e SAP", "Importazione File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        public DataSet ToDataSet(string filename, string buildingType)
        {
            using (OleDbConnection connection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + filename + "';" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'"))
            {
                try
                {
                    var table = buildingType == "Loc" ? "'SUPERFICE ATTRIBUITA$'" :
                                buildingType == "Sap" ? "'GENERALE IN PRODUZIONE$'" :
                                buildingType == "Reg" ? "'Sap_Regola_Edf$'" :
                                buildingType == "Sta" ? "'Sap_Stato_Edf$'" :
                                buildingType == "Staue" ? "'Sap_Stato_Ue$'" : "";

                    connection.Open();
                    DataSet result = new DataSet();
                    DataTable schema = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                    foreach (DataRow drSheet in schema.Rows)
                    {
                        var name = drSheet["TABLE_NAME"].ToString();
                        if (name.Contains("$") && (name == "'GENERALE IN PRODUZIONE$'") ||
                                                   name == "'SUPERFICE ATTRIBUITA$'" ||
                                                   name == "sap_regola_edf$" ||
                                                   name == "SAP_STATO_EDF$" ||
                                                   name == "SAP_STATO_UE$")
                        {
                            string s = drSheet["TABLE_NAME"].ToString();

                            if (!s.Contains("_xlnm"))
                            {

                                if (s.StartsWith("'")) s = s.Substring(1, s.Length - 2);
                                OleDbDataAdapter command = new OleDbDataAdapter($"SELECT * FROM [{s}]", connection);
                                DataTable dt = new DataTable();

                                command.Fill(dt);
                                result.Tables.Add(dt);
                            }
                            return result;
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }

            return null;
        }

        public void AggiungiOggetti()
        {
            try
            {
                var steps = refBuildingDataSet.Tables[0].Rows.Count +
                            sapreDataSet.Tables[0].Rows.Count;
                var step = steps / importBuildingPBar.Step;
                var current = 1;

                var refBuildings = new Dictionary<int, List<RefBuilding>>();
                var sapBuildings = new List<SapBuilding>();

                foreach (DataRow dr in refBuildingDataSet.Tables[0].Rows)
                {

                    if (dr["Codice edificio SAP associato"].ToString().Trim() == "" || dr["Codice CdC"].ToString().Trim() == "")
                    {
                        current++;
                        if (current > step)
                        {
                            importBuildingWorker.ReportProgress(current);
                            current = 1;
                        }
                        continue;
                    }

                    double.TryParse(dr["Superficie attribuita CDC Codice SAP"].ToString(), out var refmq);
                    double.TryParse(dr["Superficie attribuita CDC Codice Immobile"].ToString(), out var refmqCodImmob);
                    double.TryParse(dr["Percentuale Superf Codice SAP/Superf Codice Immobile"].ToString(), out var PercSup);
                    var refBuilding = new RefBuilding
                    {
                        CodiceImmobile = dr["Codice Immobile"].ToString(),
                        CodiceEdfAssociato = dr["Codice edificio SAP associato"].ToString(),
                        AiOld = dr["AI OLD"].ToString(),
                        MacroAreaNew = dr["MACROAREA NEW"].ToString(),
                        TipoImmobile = dr["Tipo Immobile"].ToString(),
                        UtilizzoPrevalente = dr["Utilizzo Prevalente"].ToString(),
                        Regione = dr["Regione"].ToString(),
                        Provincia = dr["Provincia"].ToString(),
                        Comune = dr["Comune"].ToString(),
                        //StatoGestionale = dr["Stato Gestionale"].ToString(),
                        DenominazioneImmobile = dr["Denominazione immobile"].ToString(),
                        CodiceCdc = dr["Codice CdC"].ToString(),
                        CentroDiCosto = dr["Centro di Costo"].ToString(),
                        //AreaImmobiliare = dr["Area Immobiliare / Unità organizzativa"].ToString(),
                        SuperficeAttribuitaCdcCodiceImmobile = refmqCodImmob,
                        PercSuperficeCodSap = PercSup,
                        SuperficeAttribuitaCdcCodiceSap = refmq,
                        SapBuildings = new List<SapBuilding>()
                    };

               
                    var key = $"{refBuilding.CodiceEdfAssociato}{refBuilding.CodiceCdc}".ToLower().GetHashCode();
                
                    if (!refBuildings.ContainsKey(key))
                    {
                        refBuildings.Add(key, new List<RefBuilding>());
                    }
                   
                    refBuildings[key].Add(refBuilding);


                    current++;
                    if (current > step)
                    {
                        importBuildingWorker.ReportProgress(current);
                        current = 1;
                    }
                }
                //Modifiche in seguito a nuovo file sap del 12/12/2018
                foreach (DataRow dr in sapreDataSet.Tables[0].Rows)
                {

                    if (dr["codiceIMMOBILE"].ToString().Trim() == "" || dr["cdc"].ToString().Trim() == "")
                    {
                        current++;
                        if (current > step)
                        {
                            importBuildingWorker.ReportProgress(current);
                            current = 1;
                        }
                        continue;
                    }

                    double.TryParse(dr["MQ"].ToString(), out var sapmq);
                    var sapBuilding = new SapBuilding
                    {
                        n_Ord = dr["n_ord"].ToString(),
                        StrutturaGer = dr["struttura_ger"].ToString(),
                        DENOMINSEGE = dr["DENOMIN_SEGE"].ToString(),
                        DENOMINCDC = dr["DENOMIN_CDC"].ToString(),
                        CodiceImmobile = dr["codiceIMMOBILE"].ToString(),
                        TipoMq = dr["tipo_mq"].ToString(),
                        Cdc = dr["cdc"].ToString(),
                        PeriodoScarico = dr["periodo_scarico"].ToString(),
                        Ul = dr["UL"].ToString(),
                        Mq = sapmq,
                        secoUNL4byte = dr["seco_UNL_-_4byte"].ToString(),
                        TipoDuso2Byte = dr["tipo_d_uso_-_2byte"].ToString(),
                        CodDriver10Byte = dr["cod_driver_-_10Byte"].ToString(),
                        UnitaEconomica8Byte = dr["Unità_economica_-_8Byte"].ToString(),
                        CodiceBatch = dr["CODICE BATCH"].ToString(),
                        DirDiv = dr["DIR/DIV"].ToString(),
                        RapportoEdfStrutture = dr["Rapporto edf / Strutture"].ToString(),
                        Frazionario = dr["Frazionario"].ToString(),
                        PeriodoControlloImmobile = dr["Periodo del Controllo Immobile"].ToString(),
                        QuadratureTerrenoArea = dr["Quadrature/Terreno/Area"].ToString(),

                        UEUL = dr["Unità_economica_-_8Byte"].ToString() + dr["UL"].ToString(),

                        RefBuildings = new List<RefBuilding>()
                    };

                    sapBuildings.Add(sapBuilding);

                    var key = $"{sapBuilding.CodiceImmobile}{sapBuilding.Cdc}".ToLower().GetHashCode();



                    if (refBuildings.ContainsKey(key))
                    {
                        foreach (var refBuilding in refBuildings[key])
                        {
                            refBuilding.SapBuildings.Add(sapBuilding);
                            sapBuilding.RefBuildings.Add(refBuilding);
                        }
                    }

                    current++;
                    if (current > step)
                    {
                        importBuildingWorker.ReportProgress(current);
                        current = 1;
                    }
                }

                importBuildingWorker.ReportProgress(0, $"Importazione terminata. Attendere per il salvataggio dei dati.");

                DB.RefBuildings.AddRange(refBuildings.SelectMany(kv => kv.Value).ToList());
                DB.SapBuildings.AddRange(sapBuildings);

                DB.SaveChanges();

                importBuildingWorker.ReportProgress(0, "Importazione terminata correttamente.");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void AggiungiOggettiRegole()
        {
            try
            {

                var steps = regoleSapDataSet.Tables[0].Rows.Count;
                var step = steps / importBuildingPBar.Step;
                var current = 1;

                var regoleSap = new List<RegolaSap>();
                foreach (DataRow dr in regoleSapDataSet.Tables[0].Rows)
                {

                    if (dr["UE"].ToString().Trim() == "" || dr["Edifici"].ToString().Trim() == "")
                    {
                        current++;
                        if (current > step)
                        {
                            importRegoleWorker.ReportProgress(current);
                            current = 1;
                        }
                        continue;
                    }

                    int.TryParse(dr["Cfrequiv"].ToString(), out var cfrequivint);
                    int.TryParse(dr["Damm"].ToString(), out var dammvint);
                    var regolaSap = new RegolaSap
                    {
                        Ue = dr["UE"].ToString(),
                        Edifici = dr["Edifici"].ToString(),
                        Numerooggetto = dr["Numerooggetto"].ToString(),
                        Tp = dr["Tp"].ToString(),
                        Testofabbricato = dr["Testofabbricato"].ToString(),
                        Cfrequiv = cfrequivint,
                        Damm = dammvint,
                        Daaa = dr["Daaa"].ToString(),
                        Amm = dr["Amm"].ToString(),
                        Aaa = dr["Aaa"].ToString(),
                        Noggdestsccosti = dr["Noggdestsccosti"].ToString(),
                        UL = dr["Noggdestsccosti"].ToString()
                    };
                    //!= null
                    if (string.IsNullOrEmpty(regolaSap.UL) == false && regolaSap.UL.Length > 25)
                    {
                        regolaSap.UL = regolaSap.UL.Substring(14, 8);
                    }

                    regoleSap.Add(regolaSap);

                    current++;
                    if (current > step)
                    {
                        importRegoleWorker.ReportProgress(current);
                        current = 1;
                    }
                }

                DB.RegoleSap.AddRange(regoleSap);
                DB.SaveChanges();

                importRegoleWorker.ReportProgress(0, "Attendere. Aggiornamento delle Relazioni con SapBuilding..");
                foreach (var rsap in DB.RegoleSap)
                {
                    rsap.SapBuildings = DB.SapBuildings
                                          .Where(sap => sap.UnitaEconomica8Byte == rsap.Ue)
                                          .Where(sap => sap.CodiceImmobile == rsap.Edifici)
                                          .Where(sap => sap.Ul == rsap.UL)
                                          .ToList();
                    foreach (var sap in rsap.SapBuildings)
                    {
                        sap.RegolaSap_Id = rsap.Id;
                    }
                }
                DB.SaveChanges();

                importRegoleWorker.ReportProgress(-1000, "Aggiornamento StatoEdf");

                steps = statoEdfDataSet.Tables[0].Rows.Count;
                step = steps / importBuildingPBar.Step;
                current = 1;

                var statiEdf = new List<StatoEdf>();
                foreach (DataRow dr in statoEdfDataSet.Tables[0].Rows)
                {

                    if (dr["UE"].ToString().Trim() == "" )
                    {
                        current++;
                        if (current > step)
                        {
                            importRegoleWorker.ReportProgress(current);
                            current = 1;
                        }
                        continue;
                    }
                    var statoEdf = new StatoEdf
                    {
                        UE = dr["UE"].ToString(),
                        Edifici1 = dr["Edifici_1"].ToString(),
                        Indirizzolocalità = dr["Indirizzo:località"].ToString(),
                        CAP = dr["Cap"].ToString(),
                        Indirizzoviancivico = dr["Indirizzo:via_ncivico"].ToString(),
                        Edifici2 = dr["Edifici_2"].ToString(),
                        Fabbricatotipo = dr["Fabbricato:tipo"].ToString(),
                        Statosettatodalsistema = dr["Stato:settatodalsistema"].ToString(),
                        SeCo = dr["SeCo"].ToString(),
                    };

                    statiEdf.Add(statoEdf);

                    current++;
                    if (current > step)
                    {
                        importRegoleWorker.ReportProgress(current);
                        current = 1;
                    }
                }

                DB.StatiEdf.AddRange(statiEdf);
                DB.SaveChanges();

                importRegoleWorker.ReportProgress(0, "Attendere. Aggiornamento delle Relazioni con SapBuilding..");
                foreach (var statoEdf in DB.StatiEdf)
                {
                    statoEdf.SapBuildings = DB.SapBuildings
                                              .Where(sap => sap.UnitaEconomica8Byte == statoEdf.UE)
                                              .Where(sap => sap.CodiceImmobile == statoEdf.Edifici2)
                                              .ToList();
                    foreach (var sap in statoEdf.SapBuildings)
                    {
                        sap.StatoEdf_Id = statoEdf.Id;
                    }
                }

                DB.SaveChanges();

                importRegoleWorker.ReportProgress(-1000, "Aggiornamento StatoUe");

                steps = statoUeDataSet.Tables[0].Rows.Count;
                step = steps / importBuildingPBar.Step;
                current = 1;

                var statiUe = new List<StatoUe>();
                foreach (DataRow dr in statoUeDataSet.Tables[0].Rows)
                {
                    if (dr["Unitàeconomica"].ToString().Trim() == "")
                    {
                        current++;
                        if (current > step)
                        {
                            importRegoleWorker.ReportProgress(current);
                            current = 1;
                        }
                        continue;
                    }
                    var statoUe = new StatoUe
                    {
                        Unitàeconomica = dr["Unitàeconomica"].ToString(),
                        CAP = dr["CAP"].ToString(),
                        Indirizzolocalità = dr["Indirizzo:località"].ToString(),
                        Indirizzoviancivico = dr["Indirizzo:via_ncivico"].ToString(),
                        Statosettatodalsistema = dr["Stato:settatodalsistema"].ToString(),
                    };
                    statiUe.Add(statoUe);

                    current++;
                    if (current > step)
                    {
                        importRegoleWorker.ReportProgress(current);
                        current = 1;
                    }
                }
                DB.StatiUe.AddRange(statiUe);
                DB.SaveChanges();

                importRegoleWorker.ReportProgress(0, "Attendere. Aggiornamento delle Relazioni con SapBuilding..");
                foreach (var statoUe in DB.StatiUe)
                {
                    statoUe.SapBuildings = DB.SapBuildings
                                             .Where(sap => sap.UnitaEconomica8Byte == statoUe.UE)
                                             .ToList();
                    foreach (var sap in statoUe.SapBuildings)
                    {
                        sap.StatoUe_Id = statoUe.Id;
                    }
                }
                DB.SaveChanges();

                importRegoleWorker.ReportProgress(0, "Importazione terminata correttamente.");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void ClearData(string table)
        {
            SQLiteConnection m_dbConnection;
            m_dbConnection = new SQLiteConnection(ConfigurationManager.ConnectionStrings[MainForm.CONNECTIONSTRING_NAME]
                                                                      .ConnectionString);
            m_dbConnection.Open();

            string sql = "Delete From " + table;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            m_dbConnection.Close();
            m_dbConnection.Dispose();
        }

        private void compareBuildingBtn_Click(object sender, EventArgs e)
        {
            Frm_Dati f2 = new Frm_Dati(DB, Options);
            f2.MdiParent = this.MdiParent;
            f2.Show();

            this.Close();
            this.Dispose();
        }

        private void importBuildingWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

            if (Lbl_Ref.Text != "" && Lbl_Sap.Text != "")
            {

                importBuildingWorker.ReportProgress(0, "Pulizia dei dati in corso...");

                importBuildingWorker.ReportProgress(0, $"Caricamento file {Path.GetFileName(refbuildingFilename)}...");
                refBuildingDataSet = ToDataSet(refbuildingFilename, "Loc");
                if (refBuildingDataSet == null)
                {
                    importBuildingWorker.ReportProgress(-1, "Impossibile caricare i dati dal file specificato.");
                    return;
                }

                importBuildingWorker.ReportProgress(0, $"Controllo delle superfici assegnate...");
                var invalidRows = refBuildingDataSet.Tables[0]
                                           .Select("Convert([Superficie attribuita CDC Codice SAP],'System.String')=''");

                if (invalidRows.Any())
                {
                    importBuildingWorker.ReportProgress(-1, "Attenzione,Presenti Nel File RefBuilding  valori del campo Superfice Attrbuita non valorizzata");
                    return;
                }

                importBuildingWorker.ReportProgress(0, $"Caricamento file {Path.GetFileName(sapbuildingFilename)}...");
                sapreDataSet = ToDataSet(sapbuildingFilename, "Sap");

                importBuildingWorker.ReportProgress(0, $"Lettura delle entità Ref e Sap...");

                AggiungiOggetti();

                //Scrittura in tabella LastActionSapBuilding dei file importati
                DB.LastActionRefSaps.Add(new LastActionRefSap { NameRefImport = refbuildingFilename, NameSapImport = sapbuildingFilename });
                DB.SaveChanges();

            }
        }

        private void ClearAllData()
        {
            ClearData("RefBuildings");
            ClearData("SapBuildings");
            ClearData("RegolaSaps");
            ClearData("StatoEdfs");
            ClearData("StatoUes");
            ClearData("SapBuildingsRefBuildings");
            ClearData("Sqlite_Sequence");
            ClearData("LastActionRefSaps");
        }

        private void importBuildingWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage < 0)
            {
                MessageBox.Show(this, (string)e.UserState, "Errore di Importazione", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.ProgressPercentage > 0)
            {
                importBuildingPBar.PerformStep();
            }
            else
            {
                statusLabel.Text = e.UserState.ToString();
            }
        }

        private void importBuildingWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Frm_SelectFile_Load(null, null);
        }

        private void cancelArchiveBtn_Click(object sender, EventArgs e)
        {
            ClearAllData();
            DB = new AnagraficaSapContext(MainForm.CONNECTIONSTRING_NAME);
            importBuildingBtn.Enabled = File.Exists(refbuildingFilename) &&
                                        File.Exists(sapbuildingFilename);
            compareBuildingsBtn.Enabled = false;
            archivedDataGroupBox.Visible = false;
            refFileBtn.Enabled = true;
            sapFileBtn.Enabled = true;
            importRegolaBtn.Enabled = false;
        }

        private void Frm_SelectFile_Load(object sender, EventArgs e)
        {
            cancelArchiveBtn.BackColor = System.Drawing.Color.FromArgb(244, 47, 21);

            if (DB.RefBuildings.Any())
            {
                importBuildingBtn.Enabled = false;
                compareBuildingsBtn.Enabled = true;
                refFileBtn.Enabled = false;
                sapFileBtn.Enabled = false;

                var toolStrip = new ToolTip();
                toolStrip.ToolTipIcon = ToolTipIcon.Info;
                toolStrip.ToolTipTitle = "Info File";
                toolStrip.AutoPopDelay = 10000;
                toolStrip.IsBalloon = true;
                toolStrip.ShowAlways = true;

                //delete da cancellare
                var edificiSapDelete = DB.SapBuildings
                                         .Include("RefBuildings")
                                         .Where(sap => sap.RefBuildings.Count == 0)
                                         .Count();
                labelValDaEliminare.Text = edificiSapDelete.ToString();

                //da creare
                var edificiRefCreate = DB.RefBuildings
                                         .Include("SapBuildings")
                                         .Where(refb => refb.SapBuildings.Count == 0)
                                         .Count();
                labelValDaCreare.Text = edificiRefCreate.ToString(); ;


                //congruenti
                var edificiRefequals = DB.RefBuildings
                              .Include("SapBuildings")
                              .Where(refb => refb.SapBuildings
                                                 .Select(sap => sap.Mq)
                                                 .Sum() == refb.SuperficeAttribuitaCdcCodiceSap)
                              .ToList();

                var unequalRef = DB.RefBuildings
                                   .Include("SapBuildings")
                                   .Where(refb => refb.SapBuildings
                                                      .Select(sap => sap.Mq)
                                                      .Sum() > 0 &&
                                                  refb.SapBuildings
                                                      .Select(sap => sap.Mq)
                                                      .Sum() != refb.SuperficeAttribuitaCdcCodiceSap)
                                   .ToList()
                                   .GroupBy(refb => new { refb.CodiceEdfAssociato, refb.CodiceCdc })
                                   .Where(refg => refg.Count() > 1)
                                   .Where(refg =>
                                   {
                                       var superficieRef = refg.Sum(refb => refb.SuperficeAttribuitaCdcCodiceSap);
                                       var superficieSap = refg.First().SapBuildings.Sum(sap => sap.Mq);

                                       return superficieRef == superficieSap;
                                   })
                                   .Select(refg => RefBuilding.Aggregate(refg.ToArray()));
                edificiRefequals = edificiRefequals.Union(unequalRef)
                                       .ToList();

                var Edificieaqualscount = edificiRefequals.Count();


                labelValCongruente.Text = (Edificieaqualscount).ToString();

                //da aggiornare
                var edificiRef = DB.RefBuildings
                               .Include("SapBuildings")
                               .Where(refb => refb.SapBuildings
                                                  .Select(sap => sap.Mq)
                                                  .Sum() > 0 &&
                                              refb.SapBuildings
                                                  .Select(sap => sap.Mq)
                                                  .Sum() != refb.SuperficeAttribuitaCdcCodiceSap)
                               .ToList();

                // Seleziono gli edifici per cui la somma in REF è uguale alla somma in SAP
                // E li rimuovo dagli edifici REF da aggiornare
                var removeRefbuilding = edificiRef.GroupBy(refb => new { refb.CodiceEdfAssociato, refb.CodiceCdc })
                                                  .Where(refg => refg.Count() > 1)
                                                  .Where(refg =>
                                                  {
                                                      var superficieRef = refg.Sum(refb => refb.SuperficeAttribuitaCdcCodiceSap);
                                                      var superficieSap = refg.First().SapBuildings.Sum(sap => sap.Mq);

                                                      return superficieRef == superficieSap;
                                                  })
                                                   .SelectMany(refg => refg.ToList());
                edificiRef.RemoveAll(refb => removeRefbuilding.Contains(refb));

                // Raggruppo gli edifici REF con lo stesso edificio SAP in una unica entità
                // non presente nel contesto del DB. In questo modo evitiamo di modificare la base dati
                var edificiRefcount = edificiRef.GroupBy(refb => new { refb.CodiceEdfAssociato, refb.CodiceCdc })
                                       .Select(refg => RefBuilding.Aggregate(refg.ToArray()))
                                       .Count();
                   

                labelvalDaAggiornare.Text = edificiRefcount.ToString();

                var action = DB.LastActionRefSaps.First();
                int index_ref = action.NameRefImport.LastIndexOf('\\');
                int index_sap = action.NameSapImport.LastIndexOf('\\');

                toolStrip.SetToolTip(labelValFileSap, action.NameSapImport);
                toolStrip.SetToolTip(labelValFileRef, action.NameRefImport);
                labelValFileSap.Text = action.NameSapImport.Substring(index_sap + 1);
                labelValFileRef.Text = action.NameRefImport.Substring(index_ref + 1);

                archivedDataGroupBox.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((openFileDialog1.FileName) != "")
                {
                    sapregolaFilename = openFileDialog1.FileName;
                    Lbl_Regola.Text = Path.GetFileName(sapregolaFilename);
                    importRegolaBtn.Enabled = File.Exists(sapregolaFilename);
                }
            }
        }

        private void importRegoleBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists(sapregolaFilename) && File.Exists(sapstatoFilename) && File.Exists(sapstatoUeFilename))
            {
                importBuildingBtn.Enabled = false;
                importRegolaBtn.Enabled = false;
                compareBuildingsBtn.Enabled = false;
                FileRegButton.Enabled = false;
                fileStatoEdfBtn.Enabled = false;
                fileStatoUeBtn.Enabled = false;
                importBuildingPBar.Value = 0;
                importBuildingPBar.Visible = true;

                importRegoleWorker.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show(this, "Specificare File SapRegola", "Importazione File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void FileStatoButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((openFileDialog1.FileName) != "")
                {
                    sapstatoFilename = openFileDialog1.FileName;
                    Lbl_Stato.Text = Path.GetFileName(sapstatoFilename);
                    //FileStatoButton.Enabled = File.Exists(sapstatoFilename);
                }
            }
        }

        private void FileStatoUeBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((openFileDialog1.FileName) != "")
                {
                    sapstatoUeFilename = openFileDialog1.FileName;
                    Lbl_StatoUe.Text = Path.GetFileName(sapstatoUeFilename);
                    fileStatoUeBtn.Enabled = File.Exists(sapstatoUeFilename);
                }
            }
        }

        private void importRegoleWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (Lbl_Regola.Text != "" && Lbl_Stato.Text != "" && Lbl_StatoUe.Text != "")
            {
                importRegoleWorker.ReportProgress(0, "Pulizia dei dati in corso...");

                importRegoleWorker.ReportProgress(0, $"Caricamento file {Path.GetFileName(refbuildingFilename)}...");
                regoleSapDataSet = ToDataSet(sapregolaFilename, "Reg");
                if (refBuildingDataSet == null)
                {
                    importRegoleWorker.ReportProgress(-1, "Impossibile caricare i dati dal file specificato.");
                    return;
                }

                importRegoleWorker.ReportProgress(0, $"Caricamento file {Path.GetFileName(sapbuildingFilename)}...");
                statoEdfDataSet = ToDataSet(sapstatoFilename, "Sta");
                if (statoEdfDataSet == null)
                {
                    importRegoleWorker.ReportProgress(-1, "Impossibile caricare i dati dal file specificato.");
                    return;
                }

                importRegoleWorker.ReportProgress(0, $"Caricamento file {Path.GetFileName(sapbuildingFilename)}...");
                statoUeDataSet = ToDataSet(sapstatoUeFilename, "Staue");
                if (statoUeDataSet == null)
                {
                    importRegoleWorker.ReportProgress(-1, "Impossibile caricare i dati dal file specificato.");
                    return;
                }

                importRegoleWorker.ReportProgress(0, $"Lettura delle entità Regole e Stati...");

                AggiungiOggettiRegole();
            }
        }

        private void importRegoleWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage < 0)
            {
                if (e.ProgressPercentage == -1000)
                {
                    importBuildingPBar.Value = 0;
                    statusLabel.Text = e.UserState.ToString();
                    return;
                }

                MessageBox.Show(this, (string)e.UserState, "Errore di Importazione", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (e.ProgressPercentage > 0)
            {
                importBuildingPBar.PerformStep();
            }
            else
            {
                statusLabel.Text = e.UserState.ToString();
            }
        }

        private void importRegoleWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            compareBuildingsBtn.Enabled = true;
        }

    }
}
