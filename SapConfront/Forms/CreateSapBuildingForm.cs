﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SapConfront
{
    public partial class CreateSapBuildingForm : Form
    {
        private List<CreateSapBuildingViewModel> newBuildings;

        public CreateSapBuildingForm(AnagraficaSapContext db, RefBuilding refBuilding,AnagraficaSapOptions Anagrafica)
        {
            DB = db;
            RefBuilding = refBuilding;
            anagrafica = Anagrafica;
            InitializeComponent();

            newBuildings = new List<CreateSapBuildingViewModel>();
        }

        public AnagraficaSapOptions anagrafica  { get; }

        public AnagraficaSapContext DB { get; }
        public RefBuilding RefBuilding { get; }

        private void CreateSapBuildingForm_Load(object sender, EventArgs e)
        {
            edificioLabel.Text = RefBuilding.CodiceEdfAssociato;
            centroDicostoLabel.Text = RefBuilding.CodiceCdc;
            superficieLbl.Text = $"{RefBuilding.SuperficeAttribuitaCdcCodiceSap}";

            AddSapBuildings();

            ueTBox.Enter += EnterEvent;
            ueTBox.Leave += LeaveEvent;
            ulTBox.Enter += EnterEvent;     
            ulTBox.Leave += LeaveEvent;
            tipoUsoTbox.Enter += EnterEvent;
            tipoUsoTbox.Leave += LeaveEvent;
            denomSegeTBox.Enter += EnterEvent;
            denomSegeTBox.Leave += LeaveEvent;
            secoUnlTBox.Enter += EnterEvent;
            secoUnlTBox.Leave += LeaveEvent;
            strutturaGerTBox.Enter += EnterEvent;
            strutturaGerTBox.Leave += LeaveEvent;
            codDriverTBox.Enter += EnterEvent;
            codDriverTBox.Leave += LeaveEvent;
            superficieTBox.Enter += EnterEvent;
            superficieTBox.Leave += LeaveEvent;
        }

        private void AddSapBuildings()
        {
            foreach (var building in RefBuilding.SapBuildings)
            {
                var row = new DataGridViewRow();
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.UnitaEconomica8Byte });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.Ul });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.TipoDuso2Byte });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.DENOMINSEGE });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.secoUNL4byte });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.StrutturaGer });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.CodDriver10Byte });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.Mq });


                sapBuildingsGridView.Rows.Add(row);
            }
            if (sapBuildingsGridView.RowCount > 0)
            {
                sapBuildingsGridView.CurrentCell = sapBuildingsGridView.Rows[0].Cells[4];
            }
        }
      
        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (newBuildings.Count > 0)
            {
                var sapBuildings = newBuildings.Select(b => new SapBuilding
                {
                    Cdc = RefBuilding.CodiceCdc,
                    UnitaEconomica8Byte = b.UnitaEconomica,
                    Ul = b.UnitaLocazione,
                    TipoDuso2Byte = b.TipoUso,
                    DENOMINSEGE = b.DenominazioneSege,
                    CodiceImmobile = RefBuilding.CodiceEdfAssociato,
                    secoUNL4byte = b.SecoUnl,
                    StrutturaGer = b.StrutturaGer,
                    CodDriver10Byte = b.CodiceDriver,
                    Mq = b.Superficie,
                    PeriodoScarico = anagrafica.PeriodoScaricoMese.ToString(),

                        NuovoEdificio = true,
                        RefBuildings = new List<RefBuilding> { RefBuilding }
                    }).ToArray();

                    RefBuilding.SapBuildings.RemoveAll(x=> x.CodiceImmobile == edificioLabel.Text && x.Cdc == centroDicostoLabel.Text);
                    RefBuilding.SapBuildings.AddRange(sapBuildings);
                    DB.SapBuildings.AddRange(sapBuildings);
                    DB.SaveChanges();
            }

            DialogResult = DialogResult.OK;
            Close();
        }

        private void discardBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void addSapBuilding_Click(object sender, EventArgs e)
        {

            if (!ValidateForm())
            {
                MessageBox.Show(this, "Dati Anagrafica non validi.", "Validazione Anagrafica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var building = ReadViewModel();
            newBuildings.Add(building);

            AddAnagraficaToGrid(building);
            ResetForm();
        }

        private void ResetForm()
        {
            codDriverTBox.Text =
            denomSegeTBox.Text =
            secoUnlTBox.Text =
            strutturaGerTBox.Text =
            superficieTBox.Text =
            tipoUsoTbox.Text =
            ueTBox.Text =
            ulTBox.Text = string.Empty;

            ueTBox.Focus();
        }

        private void AddAnagraficaToGrid(CreateSapBuildingViewModel building)
        {
            var row = new DataGridViewRow();
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.UnitaEconomica });
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.UnitaLocazione });
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.TipoUso });
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.DenominazioneSege });
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.SecoUnl });
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.StrutturaGer });
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.CodiceDriver });
            row.Cells.Add(new DataGridViewTextBoxCell() { Value = building.Superficie });

            sapBuildingsGridView.Rows.Add(row);
        }

        private CreateSapBuildingViewModel ReadViewModel()
        {
            var model = new CreateSapBuildingViewModel
            {
                CodiceDriver = codDriverTBox.Text,
                DenominazioneSege = denomSegeTBox.Text,
                SecoUnl = secoUnlTBox.Text,
                StrutturaGer = strutturaGerTBox.Text,
                Superficie = double.Parse(superficieTBox.Text),
                TipoUso = tipoUsoTbox.Text,
                UnitaEconomica = ueTBox.Text,
                UnitaLocazione = ulTBox.Text
            };

            return model;
        }

        public bool ValidateForm()
        {
            var valid = !string.IsNullOrWhiteSpace(codDriverTBox.Text) &&
                !string.IsNullOrWhiteSpace(denomSegeTBox.Text) &&
                !string.IsNullOrWhiteSpace(secoUnlTBox.Text) &&
                !string.IsNullOrWhiteSpace(strutturaGerTBox.Text) &&
                !string.IsNullOrWhiteSpace(superficieTBox.Text) &&
                !string.IsNullOrWhiteSpace(tipoUsoTbox.Text) &&
                !string.IsNullOrWhiteSpace(ueTBox.Text) &&
                !string.IsNullOrWhiteSpace(ulTBox.Text);

            valid = valid && double.TryParse(superficieTBox.Text, out var area);

            valid = valid && CheckNewBuildingArea();

            return valid;
        }

        private bool CheckNewBuildingArea()
        {
            double.TryParse(superficieTBox.Text, out var area);
            
            var sum = newBuildings.Sum(p => p.Superficie);
            var totalArea = area + sum;

            return RefBuilding.SuperficeAttribuitaCdcCodiceSap >= totalArea;
        }

        private void CreateSapBuildingForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode== Keys.Tab || e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                this.ProcessTabKey(true);
            }   
        }

        private System.Drawing.Color NormalColor = Color.White;
        private System.Drawing.Color FocusColor = System.Drawing.Color.Yellow;
        private void EnterEvent(object sender, EventArgs e)
        {
            if (sender is TextBox)
                ((TextBox)sender).BackColor = FocusColor;
        }

        private void LeaveEvent(object sender, EventArgs e)
        {
            if (sender is TextBox)
                ((TextBox)sender).BackColor = NormalColor;
        }

        private void sapBuildingsGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

           this.ueTBox.Text = sapBuildingsGridView["UnitaEconomica", e.RowIndex].Value.ToString();
           this.ulTBox.Text= sapBuildingsGridView["UnitaLocazione", e.RowIndex].Value.ToString();
           this.tipoUsoTbox.Text= sapBuildingsGridView["Tipouso", e.RowIndex].Value.ToString();
           this.denomSegeTBox.Text = sapBuildingsGridView["DenomSege", e.RowIndex].Value.ToString();
           this.secoUnlTBox.Text= sapBuildingsGridView["SECOUNL", e.RowIndex].Value.ToString();
           this.strutturaGerTBox.Text = sapBuildingsGridView["Struttura", e.RowIndex].Value.ToString();
           this.codDriverTBox.Text= sapBuildingsGridView["CodiceDriver", e.RowIndex].Value.ToString();
           this.superficieTBox.Text = sapBuildingsGridView["Superficie", e.RowIndex].Value.ToString();

            newBuildings.RemoveAll(x => x.UnitaEconomica == ueTBox.Text && x.UnitaLocazione == ulTBox.Text);

            sapBuildingsGridView.Rows.RemoveAt(e.RowIndex);
        }
    }
}
