﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SapConfront
{
    public partial class Frm_Dati : Form
    {
        private bool dirtyRows = false;

        public Frm_Dati(AnagraficaSapContext db, AnagraficaSapOptions anagraficaSapOptions)
        {
            InitializeComponent();
            AnagraficaSapOptions = anagraficaSapOptions;

            DB = db;
        }

        private void refSapGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn column in refSapGridView.Columns)
            {
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        public AnagraficaSapOptions AnagraficaSapOptions;
        public AnagraficaSapContext DB { get; }

        public FormDataState State { get; protected set; }


        private void QueryCongruenti(int flagfilter)
        {
            DisableForm();
            CheckDirtyRows();

            State = FormDataState.SameAreaBuildings;

            var edificiRef = DB.RefBuildings
                               .Include("SapBuildings")
                               .Where(refb => refb.SapBuildings
                                                  .Select(sap => sap.Mq)
                                                  .Sum() == refb.SuperficeAttribuitaCdcCodiceSap)
                               .ToList();

            var unequalRef = DB.RefBuildings
                               .Include("SapBuildings")
                               .Where(refb => refb.SapBuildings
                                                  .Select(sap => sap.Mq)
                                                  .Sum() > 0 &&
                                              refb.SapBuildings
                                                  .Select(sap => sap.Mq)
                                                  .Sum() != refb.SuperficeAttribuitaCdcCodiceSap)
                               .ToList()
                               .GroupBy(refb => new { refb.CodiceEdfAssociato, refb.CodiceCdc })
                               .Where(refg => refg.Count() > 1)
                               .Where(refg =>
                               {
                                   var superficieRef = refg.Sum(refb => refb.SuperficeAttribuitaCdcCodiceSap);
                                   var superficieSap = refg.First().SapBuildings.Sum(sap => sap.Mq);

                                   return superficieRef == superficieSap;
                               })
                               .Select(refg => RefBuilding.Aggregate(refg.ToArray()));
            edificiRef = edificiRef.Union(unequalRef)
                                   .ToList();

            if (flagfilter == 1)
            {
                FilterRef(edificiRef);
            }
            else
            {
                ShowRefBuildings(edificiRef);
            }


            equalBuildingBtn.BackColor = System.Drawing.Color.LightGreen;
            unequalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            createBuildingBtn.BackColor = System.Drawing.Color.Azure;
            deleteBuildingBtn.BackColor = System.Drawing.Color.Azure;
            EnableForm();

        }

        private void equalBuildingBtn_Click(object sender, EventArgs e)
        {
            stringcasefilter = "congrui";
            QueryCongruenti(0);

        }

        private void CheckDirtyRows()
        {
            if (dirtyRows)
            {
                var result = MessageBox.Show(this, "Salvare le modifiche effettuate?", "Modifiche in corso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    DB.SaveChanges();
                }
                dirtyRows = false;
            }
        }

        private void QueryElimina(int flagfilter)
        {
            DisableForm();
            CheckDirtyRows();

            State = FormDataState.DeleteBuildings;
            var edificiSap = DB.SapBuildings
                               .Include("RefBuildings")
                               .Where(sap => sap.RefBuildings.Count == 0)
                               .ToList();


            if (flagfilter == 1)
            {
                FilterSap(edificiSap);
            }
            else
            {
                ShowSapBuildings(edificiSap);
            }


            equalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            unequalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            createBuildingBtn.BackColor = System.Drawing.Color.Azure;
            deleteBuildingBtn.BackColor = System.Drawing.Color.LightGreen;
            EnableForm();
        }

        private void deleteBuildingBtn_Click(object sender, EventArgs e)
        {

            stringcasefilter = "elimina";
            QueryElimina(0);

        }

        private void unequalBuildingBtn_Click(object sender, EventArgs e)
        {
            stringcasefilter = "aggiorna";
            QueryDaAggiornare(0);
        }

        private void QueryCrea(int flagfilter)
        {
            DisableForm();
            CheckDirtyRows();

            State = FormDataState.CreateNewBuildings;
            var edificiRef = DB.RefBuildings
                               .Include("SapBuildings")
                               .Where(refb => refb.SapBuildings.Count == 0).OrderBy(x => x.CodiceEdfAssociato).ThenBy(x => x.CodiceCdc)
                               .ToList();


            if (flagfilter == 1)
            {
                FilterRef(edificiRef);
            }
            else
            {
                ShowRefBuildings(edificiRef);
            }


            equalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            unequalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            createBuildingBtn.BackColor = System.Drawing.Color.LightGreen;
            deleteBuildingBtn.BackColor = System.Drawing.Color.Azure;
            EnableForm();
        }

        private void createBuildingBtn_Click(object sender, EventArgs e)
        {
            stringcasefilter = "crea";
            QueryCrea(0);
        }

        private void ShowRefBuildings(List<RefBuilding> refBuildings)
        {
            refSapGridView.Invalidate();
            refSapGridView.DataSource = refBuildings.Select(refb => RefBuildingViewModel.FromEntity(refb))
                                                                                        .OrderBy(x => x.CodiceEDF)
                                                                                        .ThenBy(x => x.CodiceCDC)
                                                    .ToList();
            refSapGridView.Refresh();
            refSapGridView.Focus();
        }

        private void ShowSapBuildings(List<SapBuilding> sapBuildings)
        {
            refSapGridView.Invalidate();
            refSapGridView.DataSource = sapBuildings.Select(sap => SapBuildingViewModel.FromEntity(sap))
                                                    .ToList();
            refSapGridView.Refresh();
            refSapGridView.Focus();
        }

        private void refSapGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (State == FormDataState.UpdateBuildings)
            {
                UpdateSapBuildings(e.RowIndex);
            }
            else if (State == FormDataState.CreateNewBuildings)
            {
                CreateSapBuilding(e.RowIndex);
            }
        }

        //modifica in seguito al nuovo file sap 17.36
        private void salvaFileCreazioneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisableForm();
            var sapBuildings = DB.SapBuildings
                                 .Where(sap => sap.NuovoEdificio == true)
                                  .OrderBy(sap => sap.UEUL)

                                 .ToList();
            if (sapBuildings.Count == 0)
            {
                MessageBox.Show(this, "Non sono presenti edifici da creare.", "Creazione Sap", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                EnableForm();

                return;
            }

            var rows = new List<string>
            {
                sapBuildings.First().GetCreateSapUeUl(AnagraficaSapOptions)
            };

            for (var i = 1; i < sapBuildings.Count(); i++)
            {
                if ((sapBuildings.ElementAt(i).UEUL == sapBuildings.ElementAt(i - 1).UEUL))
                {
                    rows.Add(sapBuildings.ElementAt(i).GetCreateSapCdc(AnagraficaSapOptions));
                }
                else
                {
                    rows.Add(sapBuildings.ElementAt(i).GetCreateSapUeUl(AnagraficaSapOptions));
                }
            }

            var text = string.Join(Environment.NewLine, rows);

            var filename = ShowFiledialog();
            if (filename == "ko")
            {

            }
            else
            {
                File.WriteAllText(filename, text);

                MessageBox.Show(this, "Salvataggio File Crea terminata correttamente", "File Crea", MessageBoxButtons.OK, MessageBoxIcon.Information);

                foreach (var sapBuilding in sapBuildings)
                {
                    sapBuilding.NuovoEdificio = false;
                }

                DB.SaveChanges();

            }
            EnableForm();

        }
        //modifica in seguito al nuovo file sap 17.36
        private void salvaFileModificaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisableForm();
            var sapBuildings = DB.SapBuildings
                                 .Where(sap => sap.NuovoEdificio == false)
                                 .OrderBy(sap => sap.UEUL)
                                 .ToList();
            var rows = new List<string>
            {
                sapBuildings.First().GetModifySapUeUl(AnagraficaSapOptions)
            };

            for (var i = 1; i < sapBuildings.Count(); i++)
            {
                if ((sapBuildings.ElementAt(i).UEUL == sapBuildings.ElementAt(i - 1).UEUL))
                {
                    rows.Add(sapBuildings.ElementAt(i).GetModifySapCdc(AnagraficaSapOptions));
                }
                else
                {
                    rows.Add(sapBuildings.ElementAt(i).GetModifySapUeUl(AnagraficaSapOptions));
                }
            }

            var text = string.Join(Environment.NewLine, rows);

            var filename = ShowFiledialog();
            if (filename == "ko")
            {

            }
            else
            {
                File.WriteAllText(filename, text);

                MessageBox.Show(this, "Salvataggio File Modifica terminata correttamente", "File Modifica", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            EnableForm();
        }

        private void refSapGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                var rowIndex = refSapGridView.SelectedCells[0].RowIndex;
                if (State == FormDataState.UpdateBuildings)
                {
                    if (rowIndex < 0) return;

                    UpdateSapBuildings(rowIndex);
                }
                else if (State == FormDataState.CreateNewBuildings)
                {
                    if (rowIndex < 0) return;

                    CreateSapBuilding(rowIndex);
                }
            }
        }

        private void CreateSapBuilding(int rowIndex)
        {
            var building = refSapGridView.Rows[rowIndex].DataBoundItem;
            if (building is RefBuildingViewModel refBuildingVM)
            {
                var refBuilding = DB.RefBuildings
                                    .Include("SapBuildings")
                                    .Single(refb => refb.Id == refBuildingVM.Id);

                var sapForm = new CreateSapBuildingForm(DB, refBuilding, AnagraficaSapOptions);

                var result = sapForm.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    refBuildingVM.SuperficieSap = refBuilding.SapBuildings
                                                             .Select(sap => sap.Mq)
                                                             .Sum();
                    refSapGridView.InvalidateRow(rowIndex);
                }
            }
        }

        private void UpdateSapBuildings(int index)
        {
            var building = refSapGridView.Rows[index].DataBoundItem;
            if (building is RefBuildingViewModel refBuildingVM)
            {
                var refBuildings = DB.RefBuildings
                                     .Include("SapBuildings")
                                     .Where(refb => refb.CodiceEdfAssociato == refBuildingVM.CodiceEDF)
                                     .Where(refb => refb.CodiceCdc == refBuildingVM.CodiceCDC)
                                     .ToList();
                var refBuilding = RefBuilding.Aggregate(refBuildings.ToArray());

                var sapForm = new UpdateSapBuildingForm(refBuilding, DB);
                var result = sapForm.ShowDialog(this);
                if (result == DialogResult.OK)
                {

                    foreach (var refb in refBuildings)
                    {
                        DB.Entry(refb).State = System.Data.Entity.EntityState.Modified;
                    }

                    foreach (var sap in refBuilding.SapBuildings)
                    {
                        DB.Entry(sap).State = System.Data.Entity.EntityState.Modified;
                    }

                    refBuildingVM.SuperficieSap = refBuilding.SapBuildings.Select(sap => sap.Mq).Sum();

                    refSapGridView.InvalidateRow(index);

                    dirtyRows = true;
                }
            }
            else if (building is SapBuildingViewModel)
            {
                MessageBox.Show(this, "Tutti gli edifici elencati verranno eliminati da GENERALE PRODUZIONE", "Edifici SAP", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DisableForm()
        {
            createBuildingBtn.Enabled =
            deleteBuildingBtn.Enabled =
            equalBuildingBtn.Enabled =
            btn_AggAuto.Enabled =
            unequalBuildingBtn.Enabled = false;
            mainMenuStrip.Enabled = false;
        }

        private void EnableForm()
        {
            createBuildingBtn.Enabled =
            deleteBuildingBtn.Enabled =
            equalBuildingBtn.Enabled =
            btn_AggAuto.Enabled =
            unequalBuildingBtn.Enabled = true;
            mainMenuStrip.Enabled = true;
        }

        private string ShowFiledialog()
        {
            var dialog = new SaveFileDialog
            {
                Filter = "Text File|*.txt",
                Title = "Specificare il nome file"
            };

            dialog.ShowDialog(this);
            if (string.IsNullOrEmpty(dialog.FileName) == false)
            {
                return dialog.FileName;
            }
            else
            {
                return "ko";
            }
        }



        private void scriviGeneraleProdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string generaleProduzioneSheet = null;
            var dialog = new OpenFileDialog
            {
                Filter = "Excel Files|*.xls;*.xlsx",
                Title = "Generale in Produzione",
                Multiselect = false
            };

            var result = dialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                      "Data Source='" + dialog.FileName +
                      "';Extended Properties=\"Excel 12.0;HDR=YES;\"";

                var sapBuildingFields = new string[0];
                using (var connection = new OleDbConnection(connectionString))
                {
                    connection.Open();
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        var schema = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        generaleProduzioneSheet = schema.Rows
                                                    .Cast<DataRow>()
                                                    .Where(r => r["TABLE_NAME"].ToString()
                                                                               .ToLower()
                                                                               .Contains("generale in produzione"))
                                                    .Select(r => r["TABLE_NAME"].ToString())
                                                    .FirstOrDefault();

                        using (var ds = new DataSet())
                        using (var adapter = new OleDbDataAdapter($"SELECT * FROM [{generaleProduzioneSheet}]", connection))
                        {
                            adapter.Fill(ds);
                            using (var data = ds.Tables[0])
                            {
                                sapBuildingFields = data.Columns
                                                        .Cast<DataColumn>()
                                                        .Select(c => $"[{c.ColumnName}]")
                                                        .ToArray();
                                if (schema.Select().ToList().Exists(sheet => sheet["TABLE_NAME"].ToString().Contains("TEMP_PRODUZIONE")) == false)
                                {
                                    var createSapBuildingFields = data.Columns
                                                                      .Cast<DataColumn>()
                                                                      .Select(c => $"[{c.ColumnName}] char(255)")
                                                                      .ToArray();
                                    var columns = String.Join(", ", createSapBuildingFields);

                                    cmd.Connection = connection;
                                    cmd.CommandText = "CREATE TABLE [TEMP_PRODUZIONE] (" + columns + ")";
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    using (var command = new OleDbCommand { Connection = connection })
                    {
                        var entityFileds = sapBuildingFields.Where(f => ExcelEntityMapping.SapMapping.ContainsKey(f.ToLower()));
                        foreach (var sapBuilding in DB.SapBuildings)
                        {
                            var values = entityFileds
                                .Select(f =>
                                 {
                                     f = f.ToLower();
                                     var property = ExcelEntityMapping.SapMapping[f];
                                     return $"'{property.GetValue(sapBuilding)?.ToString().Replace("'", "''")}'";
                                 });

                            command.CommandText = $"Insert into [TEMP_PRODUZIONE] ({string.Join(",", entityFileds)}) values ({string.Join(",", values)})";
                            command.ExecuteNonQuery();
                        }
                        MessageBox.Show(this, "Aggiornamento File in produzione terminata correttamente", "Aggiornamento File SAP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }


        private void Frm_Dati_Load(object sender, EventArgs e)
        {
            texsearch.Enter += EnterEvent;
            texsearch.Leave += LeaveEvent;
            refSapGridView.RowsDefaultCellStyle.BackColor = System.Drawing.Color.Gainsboro;
            refSapGridView.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.Beige;

            var toolStrip = new ToolTip();
            toolStrip.ToolTipIcon = ToolTipIcon.Info;
            toolStrip.AutoPopDelay = 10000;
            toolStrip.IsBalloon = true;
            toolStrip.ShowAlways = true;

            toolStrip.SetToolTip(texsearch, "Inserire la chiave di Ricerca");
        }

        private System.Drawing.Color NormalColor = System.Drawing.Color.White;
        private System.Drawing.Color FocusColor = System.Drawing.Color.Yellow;
        private void EnterEvent(object sender, EventArgs e)
        {
            if (sender is TextBox)
                ((TextBox)sender).BackColor = FocusColor;
        }

        private void LeaveEvent(object sender, EventArgs e)
        {
            if (sender is TextBox)
                ((TextBox)sender).BackColor = NormalColor;
        }

        private void btn_AggAuto_Click(object sender, EventArgs e)
        {
            var autoUpdate = MessageBox.Show(this,
                "Stai per aggiornare le anagrafiche SAP in modalità automatica.\n" +
                "Verranno aggiornate solamente le anagrafiche con una sola corrispondenza in REF.",
                "Aggiornamento automatico SAP",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question);
            if (autoUpdate == DialogResult.OK)
            {
                try
                {
                    var autoUpdateBuildings = DB.RefBuildings
                                                .Include("SapBuildings")
                                                .Where(@ref => @ref.SapBuildings.Count == 1)
                                                .ToList();

                    var equalRefBuilding = DB.RefBuildings
                                                .Include("SapBuildings")
                                                .Where(@refb => @refb.SapBuildings.Count == 1)
                                                .ToList().GroupBy(refb => new { refb.CodiceEdfAssociato, refb.CodiceCdc })
                                                .Where(refg => refg.Count() > 1)
                                                .Select(refg => RefBuilding.Aggregate(refg.ToArray()));

                    autoUpdateBuildings = autoUpdateBuildings.Union(equalRefBuilding)
                                  .ToList();

                    foreach (var refBuilding in autoUpdateBuildings)
                    {
                       refBuilding.SapBuildings.First().Mq = refBuilding.SuperficeAttribuitaCdcCodiceSap;
                    }

                    DB.SaveChangesAsync();

                    MessageBox.Show(this, "Aggiornamento Automatico terminato correttamente.", "Aggiornamento Automatico", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show(this, "Impossibile completare l'Aggiornamento Automatico.", "Errore Imprevisto", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void creaFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisableForm();
            try
            {
                var zre13 = DB.SapBuildings
                          .Where(sap => sap.RegolaSap_Id == 0)
                          .ToList()
                          .Where(sap => !AnagraficaSapOptions.IgnoraUlInZRE13.Contains(sap.CodiceImmobile))
                          .Select(sap => $"{sap.UnitaEconomica8Byte}{sap.CodiceImmobile}{sap.Ul}{AnagraficaSapOptions.PeriodoScaricoMese.ToString().PadRight(3)}{AnagraficaSapOptions.PeriodoScaricoAnno}")
                          .ToList();
                File.WriteAllLines("Regola_EDF_ZRE13.txt", zre13);

                var zre07 = DB.StatiUe
                              .Include("SapBuildings")
                              .Where(ue => ue.Statosettatodalsistema == "RIL. OGES")
                              .SelectMany(ue => ue.SapBuildings)
                              .ToList()
                              .Select(sap => $"EPI {sap.UnitaEconomica8Byte}{sap.CodiceImmobile}{sap.Mq.ToString().PadRight(13)}{AnagraficaSapOptions.PeriodoScaricoMese.ToString().PadRight(3)}{AnagraficaSapOptions.PeriodoScaricoAnno}")
                              .ToList();
                File.WriteAllLines("Regola_UE_ZRE07.txt", zre07);

                var zre12 = DB.StatiEdf
                              .Include("SapBuildings")
                              .Where(edf => edf.Statosettatodalsistema == "RIL." ||
                                            edf.Statosettatodalsistema == "RIL. OGES" ||
                                            edf.Statosettatodalsistema == string.Empty ||
                                            edf.Statosettatodalsistema == null)
                              .SelectMany(edf => edf.SapBuildings)
                              .ToList()
                              .Select(sap => $"{sap.UnitaEconomica8Byte}{sap.CodiceImmobile}{sap.Mq.ToString().PadRight(13)}{sap.Ul}")
                              .ToList();
                File.WriteAllLines("Regola_EDF_ZRE12.txt", zre07);
                EnableForm();
                MessageBox.Show(this, "Esportazione regole terminata correttamente.", "Aggiornamento Automatico", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                new System.Exception("Errore query di esportazione regole");
            }
        }

        private void esportaStatoAttualeEdificiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisableForm();

            string filename = $"LavorazioneEdifici.{AnagraficaSapOptions.PeriodoScaricoMese}.{AnagraficaSapOptions.PeriodoScaricoAnno}.xlsx";
            File.Copy("./Templates/LavorazioneEdifici.xlsx", filename, true);

            var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                      "Data Source='" + filename +
                      "';Extended Properties=\"Excel 12.0;HDR=YES;\"";


            using (var connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = connection;
                    var createTableSapBuilding = ExcelEntityMapping.SapMapping
                                                                   .Keys
                                                                   .Select(k => $"{k} char(255)");
                    var columns = string.Join(",", createTableSapBuilding);

                    //Edifici da Creare
                    cmd.CommandText = $"CREATE TABLE [Edifici Da Creare] ({columns})";
                    cmd.ExecuteNonQuery();

                    var sapBuildings = DB.SapBuildings
                                         .Where(sap => sap.NuovoEdificio == true)
                                         .OrderBy(sap => sap.UEUL)
                                         .ToList();

                    var entityFileds = ExcelEntityMapping.SapMapping.Keys;
                    foreach (var sapBuilding in sapBuildings)
                    {
                        var values = entityFileds.Select(f =>
                        {
                            f = f.ToLower();
                            var property = ExcelEntityMapping.SapMapping[f];
                            return $"'{property.GetValue(sapBuilding)?.ToString().Replace("'", "''")}'";
                        });

                        cmd.CommandText = $"Insert into [Edifici Da Creare] ({string.Join(",", ExcelEntityMapping.SapMapping.Keys)}) values ({string.Join(",", values)})";
                        cmd.ExecuteNonQuery();
                    }

                    //Edifici da Aggiornare
                    cmd.CommandText = $"CREATE TABLE [Edifici Da Aggiornare] ({columns})";
                    cmd.ExecuteNonQuery();

                    sapBuildings = DB.SapBuildings
                                     .Where(sap => sap.NuovoEdificio == false)
                                     .OrderBy(sap => sap.UEUL)
                                     .ToList();

                    foreach (var sapBuilding in sapBuildings)
                    {
                        var values = entityFileds.Select(f =>
                                                  {
                                                      f = f.ToLower();
                                                      var property = ExcelEntityMapping.SapMapping[f];
                                                      return $"'{property.GetValue(sapBuilding)?.ToString().Replace("'", "''")}'";
                                                  });

                        cmd.CommandText = $"Insert into [Edifici Da Aggiornare] ({string.Join(",", ExcelEntityMapping.SapMapping.Keys)}) values ({string.Join(",", values)})";
                        cmd.ExecuteNonQuery();
                    }


                    //Edifici da Eliminare
                    cmd.CommandText = $"CREATE TABLE [Edifici Da Eliminare] ({columns})";
                    cmd.ExecuteNonQuery();

                    sapBuildings = DB.SapBuildings
                              .Include("RefBuildings")
                              .Where(sap => sap.RefBuildings.Count == 0)
                              .ToList();

                    foreach (var sapBuilding in sapBuildings)
                    {
                        var values = entityFileds
                                                    .Select(f =>
                                                    {
                                                        f = f.ToLower();
                                                        var property = ExcelEntityMapping.SapMapping[f];
                                                        return $"'{property.GetValue(sapBuilding)?.ToString().Replace("'", "''")}'";
                                                    });

                        cmd.CommandText = $"Insert into [Edifici Da Eliminare] ({string.Join(",", ExcelEntityMapping.SapMapping.Keys)}) values ({string.Join(",", values)})";
                        cmd.ExecuteNonQuery();
                    }

                }
            }
            EnableForm();
            MessageBox.Show(this, "Salvataggio File Riuscito", "File Lavorazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private int _previousIndex;
        private bool _sortDirection;
        private void refSapGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == _previousIndex)
                _sortDirection ^= true; // toggle direction

            refSapGridView.DataSource = SortData(
                (List<RefBuildingViewModel>)refSapGridView.DataSource, refSapGridView.Columns[e.ColumnIndex].Name, _sortDirection);

            _previousIndex = e.ColumnIndex;
        }

        public List<RefBuildingViewModel> SortData(List<RefBuildingViewModel> list, string column, bool ascending)
        {

            return ascending ?
                list.OrderBy(_ => _.GetType().GetProperty(column).GetValue(_)).ToList() :
                list.OrderByDescending(_ => _.GetType().GetProperty(column).GetValue(_)).ToList();
        }


        private void QueryDaAggiornare(int flagfilter)
        {
            DisableForm();
            CheckDirtyRows();

            State = FormDataState.UpdateBuildings;
            var edificiRef = DB.RefBuildings
                               .Include("SapBuildings")
                               .Where(refb => refb.SapBuildings
                                                  .Select(sap => sap.Mq)
                                                  .Sum() > 0 &&
                                              refb.SapBuildings
                                                  .Select(sap => sap.Mq)
                                                  .Sum() != refb.SuperficeAttribuitaCdcCodiceSap)
                               .ToList();

            // Seleziono gli edifici per cui la somma in REF è uguale alla somma in SAP
            // E li rimuovo dagli edifici REF da aggiornare
            var removeRefbuilding = edificiRef.GroupBy(refb => new { refb.CodiceEdfAssociato, refb.CodiceCdc })
                                              .Where(refg => refg.Count() > 1)
                                              .Where(refg =>
                                              {
                                                  var superficieRef = refg.Sum(refb => refb.SuperficeAttribuitaCdcCodiceSap);
                                                  var superficieSap = refg.First().SapBuildings.Sum(sap => sap.Mq);

                                                  return superficieRef == superficieSap;
                                              })
                                              .SelectMany(refg => refg.ToList());
            edificiRef.RemoveAll(refb => removeRefbuilding.Contains(refb));

            // Raggruppo gli edifici REF con lo stesso edificio SAP in una unica entità
            // non presente nel contesto del DB. In questo modo evitiamo di modificare la base dati
            edificiRef = edificiRef.GroupBy(refb => new { refb.CodiceEdfAssociato, refb.CodiceCdc })
                                   .Select(refg => RefBuilding.Aggregate(refg.ToArray()))
                                   .ToList();

            if (flagfilter == 1)
            {
                FilterRef(edificiRef);
            }
            else
            {
                ShowRefBuildings(edificiRef);
            }


            // Tutto come prima
            equalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            unequalBuildingBtn.BackColor = System.Drawing.Color.LightGreen;
            createBuildingBtn.BackColor = System.Drawing.Color.Azure;
            deleteBuildingBtn.BackColor = System.Drawing.Color.Azure;
            EnableForm();

        }

        private void FilterRef(List<RefBuilding> refBuildings)
        {
            List<RefBuilding> filtered = new List<RefBuilding>(refBuildings
                .Where(refb => refb.CodiceEdfAssociato.Contains(texsearch.Text.ToUpper()) || refb.CodiceCdc.Contains(texsearch.Text.ToUpper())).ToList());

            ShowRefBuildings(filtered);
            //equalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            //unequalBuildingBtn.BackColor = System.Drawing.Color.LightGreen;
            //createBuildingBtn.BackColor = System.Drawing.Color.Azure;
            //deleteBuildingBtn.BackColor = System.Drawing.Color.Azure;
            //EnableForm();
        }
        private void FilterSap(List<SapBuilding> sapBuildings)
        {
            List<SapBuilding> filtered = new List<SapBuilding>(sapBuildings
                .Where(refb => refb.CodiceImmobile.Contains(texsearch.Text.ToUpper()) || refb.Cdc.Contains(texsearch.Text.ToUpper())).ToList());

            ShowSapBuildings(filtered);
            //equalBuildingBtn.BackColor = System.Drawing.Color.Azure;
            //unequalBuildingBtn.BackColor = System.Drawing.Color.LightGreen;
            //createBuildingBtn.BackColor = System.Drawing.Color.Azure;
            //deleteBuildingBtn.BackColor = System.Drawing.Color.Azure;
            //EnableForm();
        }

        public string stringcasefilter;
        private void buttonsearch_Click(object sender, EventArgs e)
        {
            if (texsearch.Text.Trim() != "")
            {
                switch (stringcasefilter)
                {
                    case "aggiorna":
                        QueryDaAggiornare(1);
                        break;
                    case "congrui":
                        QueryCongruenti(1);
                        break;
                    case "crea":
                        QueryCrea(1);
                        break;
                    case "elimina":
                        QueryElimina(1);
                        break;
                    default:
                        QueryCongruenti(0);
                        break;
                }
            }
            else
            {
                QueryCongruenti(0);
            }

        }
    }
}
