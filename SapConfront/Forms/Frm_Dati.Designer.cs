﻿namespace SapConfront
{
    partial class Frm_Dati
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Dati));
            this.equalBuildingBtn = new System.Windows.Forms.Button();
            this.unequalBuildingBtn = new System.Windows.Forms.Button();
            this.createBuildingBtn = new System.Windows.Forms.Button();
            this.Grp_DatiEdificio = new System.Windows.Forms.GroupBox();
            this.GrigliaDati = new System.Windows.Forms.DataGridView();
            this.deleteBuildingBtn = new System.Windows.Forms.Button();
            this.refSapGridView = new System.Windows.Forms.DataGridView();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.salvaFileCreazioneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaFileCreazioneToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaFileModificaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.esportaRegoleSapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.aggiornaGeneraleInProduzioneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.esportaStatoAttualeEdificiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_AggAuto = new System.Windows.Forms.Button();
            this.texsearch = new System.Windows.Forms.TextBox();
            this.lblsearch = new System.Windows.Forms.Label();
            this.buttonsearch = new System.Windows.Forms.Button();
            this.Grp_DatiEdificio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrigliaDati)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refSapGridView)).BeginInit();
            this.mainMenuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // equalBuildingBtn
            // 
            this.equalBuildingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.equalBuildingBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equalBuildingBtn.Location = new System.Drawing.Point(5, 10);
            this.equalBuildingBtn.Name = "equalBuildingBtn";
            this.equalBuildingBtn.Size = new System.Drawing.Size(109, 68);
            this.equalBuildingBtn.TabIndex = 18;
            this.equalBuildingBtn.Text = "Edifici Congruenti";
            this.equalBuildingBtn.UseVisualStyleBackColor = true;
            this.equalBuildingBtn.Click += new System.EventHandler(this.equalBuildingBtn_Click);
            // 
            // unequalBuildingBtn
            // 
            this.unequalBuildingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.unequalBuildingBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unequalBuildingBtn.Location = new System.Drawing.Point(143, 10);
            this.unequalBuildingBtn.Name = "unequalBuildingBtn";
            this.unequalBuildingBtn.Size = new System.Drawing.Size(103, 68);
            this.unequalBuildingBtn.TabIndex = 20;
            this.unequalBuildingBtn.Text = "Edifici da Aggiornare";
            this.unequalBuildingBtn.UseVisualStyleBackColor = true;
            this.unequalBuildingBtn.Click += new System.EventHandler(this.unequalBuildingBtn_Click);
            // 
            // createBuildingBtn
            // 
            this.createBuildingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.createBuildingBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createBuildingBtn.Location = new System.Drawing.Point(274, 10);
            this.createBuildingBtn.Name = "createBuildingBtn";
            this.createBuildingBtn.Size = new System.Drawing.Size(103, 68);
            this.createBuildingBtn.TabIndex = 21;
            this.createBuildingBtn.Text = "Edifici da Creare";
            this.createBuildingBtn.UseVisualStyleBackColor = true;
            this.createBuildingBtn.Click += new System.EventHandler(this.createBuildingBtn_Click);
            // 
            // Grp_DatiEdificio
            // 
            this.Grp_DatiEdificio.BackColor = System.Drawing.SystemColors.MenuBar;
            this.Grp_DatiEdificio.Controls.Add(this.GrigliaDati);
            this.Grp_DatiEdificio.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grp_DatiEdificio.Location = new System.Drawing.Point(3, 586);
            this.Grp_DatiEdificio.Name = "Grp_DatiEdificio";
            this.Grp_DatiEdificio.Size = new System.Drawing.Size(1004, 118);
            this.Grp_DatiEdificio.TabIndex = 22;
            this.Grp_DatiEdificio.TabStop = false;
            this.Grp_DatiEdificio.Text = "Dati Edificio";
            // 
            // GrigliaDati
            // 
            this.GrigliaDati.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrigliaDati.Location = new System.Drawing.Point(9, 24);
            this.GrigliaDati.Name = "GrigliaDati";
            this.GrigliaDati.Size = new System.Drawing.Size(989, 83);
            this.GrigliaDati.TabIndex = 0;
            // 
            // deleteBuildingBtn
            // 
            this.deleteBuildingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.deleteBuildingBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteBuildingBtn.Location = new System.Drawing.Point(406, 10);
            this.deleteBuildingBtn.Name = "deleteBuildingBtn";
            this.deleteBuildingBtn.Size = new System.Drawing.Size(103, 68);
            this.deleteBuildingBtn.TabIndex = 19;
            this.deleteBuildingBtn.Text = "Edifici da Eliminare";
            this.deleteBuildingBtn.UseVisualStyleBackColor = true;
            this.deleteBuildingBtn.Click += new System.EventHandler(this.deleteBuildingBtn_Click);
            // 
            // refSapGridView
            // 
            this.refSapGridView.AllowUserToAddRows = false;
            this.refSapGridView.AllowUserToDeleteRows = false;
            this.refSapGridView.AllowUserToOrderColumns = true;
            this.refSapGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.refSapGridView.Location = new System.Drawing.Point(2, 64);
            this.refSapGridView.Margin = new System.Windows.Forms.Padding(2);
            this.refSapGridView.MultiSelect = false;
            this.refSapGridView.Name = "refSapGridView";
            this.refSapGridView.ReadOnly = true;
            this.refSapGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.refSapGridView.RowTemplate.Height = 28;
            this.refSapGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.refSapGridView.Size = new System.Drawing.Size(682, 320);
            this.refSapGridView.TabIndex = 23;
            this.refSapGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.refSapGridView_CellDoubleClick);
            this.refSapGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.refSapGridView_ColumnHeaderMouseClick);
            this.refSapGridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.refSapGridView_DataBindingComplete);
            this.refSapGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.refSapGridView_KeyDown);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mainMenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salvaFileCreazioneToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.mainMenuStrip.Size = new System.Drawing.Size(685, 30);
            this.mainMenuStrip.TabIndex = 24;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // salvaFileCreazioneToolStripMenuItem
            // 
            this.salvaFileCreazioneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salvaFileCreazioneToolStripMenuItem1,
            this.salvaFileModificaToolStripMenuItem1,
            this.toolStripSeparator1,
            this.esportaRegoleSapToolStripMenuItem,
            this.toolStripSeparator2,
            this.aggiornaGeneraleInProduzioneToolStripMenuItem,
            this.toolStripSeparator3,
            this.esportaStatoAttualeEdificiToolStripMenuItem});
            this.salvaFileCreazioneToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salvaFileCreazioneToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("salvaFileCreazioneToolStripMenuItem.Image")));
            this.salvaFileCreazioneToolStripMenuItem.Name = "salvaFileCreazioneToolStripMenuItem";
            this.salvaFileCreazioneToolStripMenuItem.Size = new System.Drawing.Size(116, 28);
            this.salvaFileCreazioneToolStripMenuItem.Text = "Esportazioni";
            // 
            // salvaFileCreazioneToolStripMenuItem1
            // 
            this.salvaFileCreazioneToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("salvaFileCreazioneToolStripMenuItem1.Image")));
            this.salvaFileCreazioneToolStripMenuItem1.Name = "salvaFileCreazioneToolStripMenuItem1";
            this.salvaFileCreazioneToolStripMenuItem1.Size = new System.Drawing.Size(270, 22);
            this.salvaFileCreazioneToolStripMenuItem1.Text = "Salva File Creazione";
            this.salvaFileCreazioneToolStripMenuItem1.Click += new System.EventHandler(this.salvaFileCreazioneToolStripMenuItem_Click);
            // 
            // salvaFileModificaToolStripMenuItem1
            // 
            this.salvaFileModificaToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("salvaFileModificaToolStripMenuItem1.Image")));
            this.salvaFileModificaToolStripMenuItem1.Name = "salvaFileModificaToolStripMenuItem1";
            this.salvaFileModificaToolStripMenuItem1.Size = new System.Drawing.Size(270, 22);
            this.salvaFileModificaToolStripMenuItem1.Text = "Salva File Modifica";
            this.salvaFileModificaToolStripMenuItem1.Click += new System.EventHandler(this.salvaFileModificaToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(267, 6);
            // 
            // esportaRegoleSapToolStripMenuItem
            // 
            this.esportaRegoleSapToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("esportaRegoleSapToolStripMenuItem.Image")));
            this.esportaRegoleSapToolStripMenuItem.Name = "esportaRegoleSapToolStripMenuItem";
            this.esportaRegoleSapToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.esportaRegoleSapToolStripMenuItem.Text = "Esporta Regole Sap";
            this.esportaRegoleSapToolStripMenuItem.Click += new System.EventHandler(this.creaFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(267, 6);
            // 
            // aggiornaGeneraleInProduzioneToolStripMenuItem
            // 
            this.aggiornaGeneraleInProduzioneToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aggiornaGeneraleInProduzioneToolStripMenuItem.Image")));
            this.aggiornaGeneraleInProduzioneToolStripMenuItem.Name = "aggiornaGeneraleInProduzioneToolStripMenuItem";
            this.aggiornaGeneraleInProduzioneToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.aggiornaGeneraleInProduzioneToolStripMenuItem.Text = "Aggiorna Generale in Produzione";
            this.aggiornaGeneraleInProduzioneToolStripMenuItem.Click += new System.EventHandler(this.scriviGeneraleProdToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(267, 6);
            // 
            // esportaStatoAttualeEdificiToolStripMenuItem
            // 
            this.esportaStatoAttualeEdificiToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("esportaStatoAttualeEdificiToolStripMenuItem.Image")));
            this.esportaStatoAttualeEdificiToolStripMenuItem.Name = "esportaStatoAttualeEdificiToolStripMenuItem";
            this.esportaStatoAttualeEdificiToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.esportaStatoAttualeEdificiToolStripMenuItem.Text = "Esporta Stato Attuale Edifici";
            this.esportaStatoAttualeEdificiToolStripMenuItem.Click += new System.EventHandler(this.esportaStatoAttualeEdificiToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Azure;
            this.groupBox1.Controls.Add(this.btn_AggAuto);
            this.groupBox1.Controls.Add(this.equalBuildingBtn);
            this.groupBox1.Controls.Add(this.deleteBuildingBtn);
            this.groupBox1.Controls.Add(this.unequalBuildingBtn);
            this.groupBox1.Controls.Add(this.createBuildingBtn);
            this.groupBox1.Location = new System.Drawing.Point(7, 385);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(677, 84);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            // 
            // btn_AggAuto
            // 
            this.btn_AggAuto.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_AggAuto.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AggAuto.Location = new System.Drawing.Point(536, 10);
            this.btn_AggAuto.Name = "btn_AggAuto";
            this.btn_AggAuto.Size = new System.Drawing.Size(131, 68);
            this.btn_AggAuto.TabIndex = 22;
            this.btn_AggAuto.Text = "Aggiornamento Automatico";
            this.btn_AggAuto.UseVisualStyleBackColor = true;
            this.btn_AggAuto.Click += new System.EventHandler(this.btn_AggAuto_Click);
            // 
            // texsearch
            // 
            this.texsearch.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.texsearch.Location = new System.Drawing.Point(69, 35);
            this.texsearch.Name = "texsearch";
            this.texsearch.Size = new System.Drawing.Size(112, 25);
            this.texsearch.TabIndex = 26;
            // 
            // lblsearch
            // 
            this.lblsearch.AutoSize = true;
            this.lblsearch.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsearch.Location = new System.Drawing.Point(12, 37);
            this.lblsearch.Name = "lblsearch";
            this.lblsearch.Size = new System.Drawing.Size(53, 20);
            this.lblsearch.TabIndex = 27;
            this.lblsearch.Text = "Ricerca";
            // 
            // buttonsearch
            // 
            this.buttonsearch.BackColor = System.Drawing.Color.GreenYellow;
            this.buttonsearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonsearch.BackgroundImage")));
            this.buttonsearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonsearch.Location = new System.Drawing.Point(185, 36);
            this.buttonsearch.Name = "buttonsearch";
            this.buttonsearch.Size = new System.Drawing.Size(35, 23);
            this.buttonsearch.TabIndex = 28;
            this.buttonsearch.UseVisualStyleBackColor = false;
            this.buttonsearch.Click += new System.EventHandler(this.buttonsearch_Click);
            // 
            // Frm_Dati
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 479);
            this.Controls.Add(this.buttonsearch);
            this.Controls.Add(this.lblsearch);
            this.Controls.Add(this.texsearch);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.refSapGridView);
            this.Controls.Add(this.Grp_DatiEdificio);
            this.Controls.Add(this.mainMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Dati";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AnagraficaImmobile-Inserimento Dati";
            this.Load += new System.EventHandler(this.Frm_Dati_Load);
            this.Grp_DatiEdificio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrigliaDati)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refSapGridView)).EndInit();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button equalBuildingBtn;
        private System.Windows.Forms.Button unequalBuildingBtn;
        private System.Windows.Forms.Button createBuildingBtn;
        private System.Windows.Forms.GroupBox Grp_DatiEdificio;
        private System.Windows.Forms.DataGridView GrigliaDati;
        private System.Windows.Forms.Button deleteBuildingBtn;
        private System.Windows.Forms.DataGridView refSapGridView;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem salvaFileCreazioneToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_AggAuto;
        private System.Windows.Forms.ToolStripMenuItem salvaFileCreazioneToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salvaFileModificaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aggiornaGeneraleInProduzioneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esportaRegoleSapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esportaStatoAttualeEdificiToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.TextBox texsearch;
        private System.Windows.Forms.Label lblsearch;
        private System.Windows.Forms.Button buttonsearch;
    }
}