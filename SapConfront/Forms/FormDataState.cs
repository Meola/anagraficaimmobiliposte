﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SapConfront
{
    public enum FormDataState
    {
        None,

        SameAreaBuildings,
        UpdateBuildings,
        CreateNewBuildings,
        DeleteBuildings
    }
}
