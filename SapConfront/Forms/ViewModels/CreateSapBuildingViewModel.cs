﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SapConfront
{
    public class CreateSapBuildingViewModel
    {
        public string UnitaEconomica { get; set; }
        public string UnitaLocazione { get; set; }
        public string TipoUso { get; set; }

        public string DenominazioneSege { get; set; }

        public string SecoUnl { get; set; }
        public string StrutturaGer { get; set; }
        public string CodiceDriver { get; set; }

        public double Superficie { get; set; }
    }
}
