﻿using System.Linq;

namespace SapConfront
{
    public class RefBuildingViewModel
    {
        public int Id { get; set; }

        public string CodiceEDF { get; set; }

        public string CodiceCDC { get; set; }

        public double SuperficieRef { get; set; }

        public double SuperficieSap { get; set; }

        public static RefBuildingViewModel FromEntity(RefBuilding entity)
        {
            return new RefBuildingViewModel
            {
                Id = entity.Id,
                CodiceCDC = entity.CodiceCdc,
                CodiceEDF = entity.CodiceEdfAssociato,
                SuperficieRef = entity.SuperficeAttribuitaCdcCodiceSap,
                SuperficieSap = entity.SapBuildings.Select(sap => sap.Mq).Sum()
            };
        }
    }
}
