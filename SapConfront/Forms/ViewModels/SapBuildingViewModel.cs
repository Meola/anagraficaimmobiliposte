﻿namespace SapConfront
{
    public class SapBuildingViewModel
    {
        public int Id { get; set; }

        public string CodiceEDF { get; set; }
        public string CodiceCDC { get; set; }

        public string UnitaEconomica { get; set; }
        public string UnitàLocazione { get; set; }

        public double SuperficieSap { get; set; }

        public static SapBuildingViewModel FromEntity(SapBuilding entity)
        {
            return new SapBuildingViewModel
            {
                Id = entity.Id,
                CodiceCDC = entity.Cdc,
                CodiceEDF = entity.CodiceImmobile,
                UnitaEconomica = entity.UnitaEconomica8Byte,
                UnitàLocazione = entity.Ul,
                SuperficieSap = entity.Mq
            };
        }
    }
}
