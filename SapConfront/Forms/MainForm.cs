﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SapConfront
{
    public partial class MainForm : Form
    {
        public static readonly string CONNECTIONSTRING_NAME = "RefBuildingContext";

        public MainForm()
        {
            InitializeComponent();

            DB = new AnagraficaSapContext(CONNECTIONSTRING_NAME);
        }

        public AnagraficaSapContext DB { get; }

        private void toolStripButtonSelezionafile_Click(object sender, EventArgs e)
        {
            Frm_SelectFile child = new Frm_SelectFile(DB)
            {
                MdiParent = this
            };
            child.Show();
        }

        private void toolStripButtonExit_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void toolStripButtonOpzioni_Click(object sender, EventArgs e)
        {
            FrmOptions child = new FrmOptions(DB)
            {
                MdiParent = this
            };
            child.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Controls.OfType<MdiClient>().FirstOrDefault().BackColor = Color.FromArgb(238, 247, 2);
        }

        private void toolstripSalvatutto_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show(this, "Salvare le modifiche effettuate?", "Modifiche in corso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                DB.SaveChanges();
            }
        }
    }
}
