﻿namespace SapConfront
{
    partial class CreateSapBuildingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateSapBuildingForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edificioLabel = new System.Windows.Forms.Label();
            this.centroDicostoLabel = new System.Windows.Forms.Label();
            this.anagraficaRefGBox = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.superficieLbl = new System.Windows.Forms.Label();
            this.anagraficaSapGBox = new System.Windows.Forms.GroupBox();
            this.sapBuildingsGridView = new System.Windows.Forms.DataGridView();
            this.UnitaEconomica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitaLocazione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoUso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DenomSege = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SecoUnl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Struttura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodiceDriver = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Superficie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addSapBuilding = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.superficieTBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.codDriverTBox = new System.Windows.Forms.TextBox();
            this.strutturaGerTBox = new System.Windows.Forms.TextBox();
            this.secoUnlTBox = new System.Windows.Forms.TextBox();
            this.denomSegeTBox = new System.Windows.Forms.TextBox();
            this.tipoUsoTbox = new System.Windows.Forms.TextBox();
            this.ulTBox = new System.Windows.Forms.TextBox();
            this.ueTBox = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.discardBtn = new System.Windows.Forms.Button();
            this.anagraficaRefGBox.SuspendLayout();
            this.anagraficaSapGBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sapBuildingsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Edificio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(209, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Centro Di Costo";
            // 
            // edificioLabel
            // 
            this.edificioLabel.AutoSize = true;
            this.edificioLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edificioLabel.Location = new System.Drawing.Point(47, 22);
            this.edificioLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.edificioLabel.Name = "edificioLabel";
            this.edificioLabel.Size = new System.Drawing.Size(39, 13);
            this.edificioLabel.TabIndex = 2;
            this.edificioLabel.Text = "        ";
            // 
            // centroDicostoLabel
            // 
            this.centroDicostoLabel.AutoSize = true;
            this.centroDicostoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centroDicostoLabel.Location = new System.Drawing.Point(294, 22);
            this.centroDicostoLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.centroDicostoLabel.Name = "centroDicostoLabel";
            this.centroDicostoLabel.Size = new System.Drawing.Size(39, 13);
            this.centroDicostoLabel.TabIndex = 3;
            this.centroDicostoLabel.Text = "        ";
            // 
            // anagraficaRefGBox
            // 
            this.anagraficaRefGBox.Controls.Add(this.label3);
            this.anagraficaRefGBox.Controls.Add(this.superficieLbl);
            this.anagraficaRefGBox.Controls.Add(this.label2);
            this.anagraficaRefGBox.Controls.Add(this.centroDicostoLabel);
            this.anagraficaRefGBox.Controls.Add(this.label1);
            this.anagraficaRefGBox.Controls.Add(this.edificioLabel);
            this.anagraficaRefGBox.Location = new System.Drawing.Point(4, 8);
            this.anagraficaRefGBox.Margin = new System.Windows.Forms.Padding(2);
            this.anagraficaRefGBox.Name = "anagraficaRefGBox";
            this.anagraficaRefGBox.Padding = new System.Windows.Forms.Padding(2);
            this.anagraficaRefGBox.Size = new System.Drawing.Size(531, 54);
            this.anagraficaRefGBox.TabIndex = 4;
            this.anagraficaRefGBox.TabStop = false;
            this.anagraficaRefGBox.Text = "Anagrafica REF";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(419, 22);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Superficie";
            // 
            // superficieLbl
            // 
            this.superficieLbl.AutoSize = true;
            this.superficieLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.superficieLbl.Location = new System.Drawing.Point(476, 22);
            this.superficieLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.superficieLbl.Name = "superficieLbl";
            this.superficieLbl.Size = new System.Drawing.Size(39, 13);
            this.superficieLbl.TabIndex = 5;
            this.superficieLbl.Text = "        ";
            // 
            // anagraficaSapGBox
            // 
            this.anagraficaSapGBox.Controls.Add(this.sapBuildingsGridView);
            this.anagraficaSapGBox.Controls.Add(this.addSapBuilding);
            this.anagraficaSapGBox.Controls.Add(this.label13);
            this.anagraficaSapGBox.Controls.Add(this.superficieTBox);
            this.anagraficaSapGBox.Controls.Add(this.label12);
            this.anagraficaSapGBox.Controls.Add(this.label11);
            this.anagraficaSapGBox.Controls.Add(this.label10);
            this.anagraficaSapGBox.Controls.Add(this.label9);
            this.anagraficaSapGBox.Controls.Add(this.label7);
            this.anagraficaSapGBox.Controls.Add(this.label6);
            this.anagraficaSapGBox.Controls.Add(this.label5);
            this.anagraficaSapGBox.Controls.Add(this.label4);
            this.anagraficaSapGBox.Controls.Add(this.codDriverTBox);
            this.anagraficaSapGBox.Controls.Add(this.strutturaGerTBox);
            this.anagraficaSapGBox.Controls.Add(this.secoUnlTBox);
            this.anagraficaSapGBox.Controls.Add(this.denomSegeTBox);
            this.anagraficaSapGBox.Controls.Add(this.tipoUsoTbox);
            this.anagraficaSapGBox.Controls.Add(this.ulTBox);
            this.anagraficaSapGBox.Controls.Add(this.ueTBox);
            this.anagraficaSapGBox.Location = new System.Drawing.Point(4, 66);
            this.anagraficaSapGBox.Margin = new System.Windows.Forms.Padding(2);
            this.anagraficaSapGBox.Name = "anagraficaSapGBox";
            this.anagraficaSapGBox.Padding = new System.Windows.Forms.Padding(2);
            this.anagraficaSapGBox.Size = new System.Drawing.Size(529, 330);
            this.anagraficaSapGBox.TabIndex = 5;
            this.anagraficaSapGBox.TabStop = false;
            this.anagraficaSapGBox.Text = "Anagrafica SAP";
            // 
            // sapBuildingsGridView
            // 
            this.sapBuildingsGridView.AllowUserToAddRows = false;
            this.sapBuildingsGridView.AllowUserToDeleteRows = false;
            this.sapBuildingsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sapBuildingsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UnitaEconomica,
            this.UnitaLocazione,
            this.TipoUso,
            this.DenomSege,
            this.SecoUnl,
            this.Struttura,
            this.CodiceDriver,
            this.Superficie});
            this.sapBuildingsGridView.Location = new System.Drawing.Point(4, 164);
            this.sapBuildingsGridView.Margin = new System.Windows.Forms.Padding(2);
            this.sapBuildingsGridView.MultiSelect = false;
            this.sapBuildingsGridView.Name = "sapBuildingsGridView";
            this.sapBuildingsGridView.ReadOnly = true;
            this.sapBuildingsGridView.RowHeadersVisible = false;
            this.sapBuildingsGridView.RowTemplate.Height = 28;
            this.sapBuildingsGridView.Size = new System.Drawing.Size(509, 161);
            this.sapBuildingsGridView.TabIndex = 18;
            this.sapBuildingsGridView.TabStop = false;
            this.sapBuildingsGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sapBuildingsGridView_CellDoubleClick);
            // 
            // UnitaEconomica
            // 
            this.UnitaEconomica.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UnitaEconomica.HeaderText = "Unità Economica";
            this.UnitaEconomica.Name = "UnitaEconomica";
            this.UnitaEconomica.ReadOnly = true;
            // 
            // UnitaLocazione
            // 
            this.UnitaLocazione.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UnitaLocazione.HeaderText = "Unità di Locazione";
            this.UnitaLocazione.Name = "UnitaLocazione";
            this.UnitaLocazione.ReadOnly = true;
            // 
            // TipoUso
            // 
            this.TipoUso.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TipoUso.HeaderText = "Tipo d\'uso";
            this.TipoUso.Name = "TipoUso";
            this.TipoUso.ReadOnly = true;
            // 
            // DenomSege
            // 
            this.DenomSege.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DenomSege.HeaderText = "Denominazione SEGE";
            this.DenomSege.Name = "DenomSege";
            this.DenomSege.ReadOnly = true;
            // 
            // SecoUnl
            // 
            this.SecoUnl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SecoUnl.HeaderText = "SECO UNL";
            this.SecoUnl.Name = "SecoUnl";
            this.SecoUnl.ReadOnly = true;
            // 
            // Struttura
            // 
            this.Struttura.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Struttura.HeaderText = "Struttura Ger";
            this.Struttura.Name = "Struttura";
            this.Struttura.ReadOnly = true;
            // 
            // CodiceDriver
            // 
            this.CodiceDriver.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CodiceDriver.HeaderText = "Codice Driver";
            this.CodiceDriver.Name = "CodiceDriver";
            this.CodiceDriver.ReadOnly = true;
            // 
            // Superficie
            // 
            this.Superficie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Superficie.HeaderText = "Superficie";
            this.Superficie.Name = "Superficie";
            this.Superficie.ReadOnly = true;
            // 
            // addSapBuilding
            // 
            this.addSapBuilding.BackColor = System.Drawing.Color.Azure;
            this.addSapBuilding.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addSapBuilding.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSapBuilding.Location = new System.Drawing.Point(371, 117);
            this.addSapBuilding.Margin = new System.Windows.Forms.Padding(2);
            this.addSapBuilding.Name = "addSapBuilding";
            this.addSapBuilding.Size = new System.Drawing.Size(137, 39);
            this.addSapBuilding.TabIndex = 12;
            this.addSapBuilding.Text = "Aggiungi Anagrafica";
            this.addSapBuilding.UseVisualStyleBackColor = false;
            this.addSapBuilding.Click += new System.EventHandler(this.addSapBuilding_Click);
            // 
            // label13
            // 
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Location = new System.Drawing.Point(4, 161);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(509, 1);
            this.label13.TabIndex = 17;
            // 
            // superficieTBox
            // 
            this.superficieTBox.Location = new System.Drawing.Point(472, 84);
            this.superficieTBox.Margin = new System.Windows.Forms.Padding(2);
            this.superficieTBox.MaxLength = 7;
            this.superficieTBox.Name = "superficieTBox";
            this.superficieTBox.Size = new System.Drawing.Size(37, 20);
            this.superficieTBox.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(415, 86);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Superficie";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(282, 86);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Cod. Driver";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(139, 86);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Struttura Ger";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 86);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "SECO UNL";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 52);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Denom. SEGE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(384, 18);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Tipo d\'uso";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(193, 18);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Unità di Locazione";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 18);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Unità Economica";
            // 
            // codDriverTBox
            // 
            this.codDriverTBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.codDriverTBox.Location = new System.Drawing.Point(344, 84);
            this.codDriverTBox.Margin = new System.Windows.Forms.Padding(2);
            this.codDriverTBox.MaxLength = 10;
            this.codDriverTBox.Name = "codDriverTBox";
            this.codDriverTBox.Size = new System.Drawing.Size(68, 20);
            this.codDriverTBox.TabIndex = 7;
            // 
            // strutturaGerTBox
            // 
            this.strutturaGerTBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.strutturaGerTBox.Location = new System.Drawing.Point(211, 84);
            this.strutturaGerTBox.Margin = new System.Windows.Forms.Padding(2);
            this.strutturaGerTBox.MaxLength = 20;
            this.strutturaGerTBox.Name = "strutturaGerTBox";
            this.strutturaGerTBox.Size = new System.Drawing.Size(68, 20);
            this.strutturaGerTBox.TabIndex = 6;
            // 
            // secoUnlTBox
            // 
            this.secoUnlTBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.secoUnlTBox.Location = new System.Drawing.Point(68, 84);
            this.secoUnlTBox.Margin = new System.Windows.Forms.Padding(2);
            this.secoUnlTBox.MaxLength = 4;
            this.secoUnlTBox.Name = "secoUnlTBox";
            this.secoUnlTBox.Size = new System.Drawing.Size(68, 20);
            this.secoUnlTBox.TabIndex = 5;
            // 
            // denomSegeTBox
            // 
            this.denomSegeTBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.denomSegeTBox.Location = new System.Drawing.Point(95, 50);
            this.denomSegeTBox.Margin = new System.Windows.Forms.Padding(2);
            this.denomSegeTBox.MaxLength = 30;
            this.denomSegeTBox.Name = "denomSegeTBox";
            this.denomSegeTBox.Size = new System.Drawing.Size(413, 20);
            this.denomSegeTBox.TabIndex = 3;
            // 
            // tipoUsoTbox
            // 
            this.tipoUsoTbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tipoUsoTbox.Location = new System.Drawing.Point(442, 16);
            this.tipoUsoTbox.Margin = new System.Windows.Forms.Padding(2);
            this.tipoUsoTbox.MaxLength = 2;
            this.tipoUsoTbox.Name = "tipoUsoTbox";
            this.tipoUsoTbox.Size = new System.Drawing.Size(68, 20);
            this.tipoUsoTbox.TabIndex = 2;
            // 
            // ulTBox
            // 
            this.ulTBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ulTBox.Location = new System.Drawing.Point(290, 16);
            this.ulTBox.Margin = new System.Windows.Forms.Padding(2);
            this.ulTBox.MaxLength = 8;
            this.ulTBox.Name = "ulTBox";
            this.ulTBox.Size = new System.Drawing.Size(68, 20);
            this.ulTBox.TabIndex = 1;
            // 
            // ueTBox
            // 
            this.ueTBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ueTBox.Location = new System.Drawing.Point(95, 16);
            this.ueTBox.Margin = new System.Windows.Forms.Padding(2);
            this.ueTBox.MaxLength = 8;
            this.ueTBox.Name = "ueTBox";
            this.ueTBox.Size = new System.Drawing.Size(68, 20);
            this.ueTBox.TabIndex = 0;
            // 
            // saveBtn
            // 
            this.saveBtn.BackColor = System.Drawing.Color.Azure;
            this.saveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.saveBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Location = new System.Drawing.Point(458, 400);
            this.saveBtn.Margin = new System.Windows.Forms.Padding(2);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(60, 41);
            this.saveBtn.TabIndex = 6;
            this.saveBtn.Text = "Salva";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // discardBtn
            // 
            this.discardBtn.BackColor = System.Drawing.Color.Azure;
            this.discardBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.discardBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discardBtn.Location = new System.Drawing.Point(388, 400);
            this.discardBtn.Margin = new System.Windows.Forms.Padding(2);
            this.discardBtn.Name = "discardBtn";
            this.discardBtn.Size = new System.Drawing.Size(60, 41);
            this.discardBtn.TabIndex = 7;
            this.discardBtn.Text = "Annulla";
            this.discardBtn.UseVisualStyleBackColor = false;
            this.discardBtn.Click += new System.EventHandler(this.discardBtn_Click);
            // 
            // CreateSapBuildingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 444);
            this.Controls.Add(this.discardBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.anagraficaSapGBox);
            this.Controls.Add(this.anagraficaRefGBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateSapBuildingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Creazione Nuova Anagrafica SAP";
            this.Load += new System.EventHandler(this.CreateSapBuildingForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CreateSapBuildingForm_KeyDown);
            this.anagraficaRefGBox.ResumeLayout(false);
            this.anagraficaRefGBox.PerformLayout();
            this.anagraficaSapGBox.ResumeLayout(false);
            this.anagraficaSapGBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sapBuildingsGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label edificioLabel;
        private System.Windows.Forms.Label centroDicostoLabel;
        private System.Windows.Forms.GroupBox anagraficaRefGBox;
        private System.Windows.Forms.GroupBox anagraficaSapGBox;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label superficieLbl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox codDriverTBox;
        private System.Windows.Forms.TextBox strutturaGerTBox;
        private System.Windows.Forms.TextBox secoUnlTBox;
        private System.Windows.Forms.TextBox denomSegeTBox;
        private System.Windows.Forms.TextBox tipoUsoTbox;
        private System.Windows.Forms.TextBox ulTBox;
        private System.Windows.Forms.TextBox ueTBox;
        private System.Windows.Forms.DataGridView sapBuildingsGridView;
        private System.Windows.Forms.Button addSapBuilding;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox superficieTBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button discardBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitaEconomica;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitaLocazione;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoUso;
        private System.Windows.Forms.DataGridViewTextBoxColumn DenomSege;
        private System.Windows.Forms.DataGridViewTextBoxColumn SecoUnl;
        private System.Windows.Forms.DataGridViewTextBoxColumn Struttura;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodiceDriver;
        private System.Windows.Forms.DataGridViewTextBoxColumn Superficie;
    }
}