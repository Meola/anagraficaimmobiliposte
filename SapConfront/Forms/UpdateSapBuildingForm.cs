﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
namespace SapConfront
{

    public partial class UpdateSapBuildingForm : Form
    {
        public AnagraficaSapContext DB { get; }
        public UpdateSapBuildingForm(RefBuilding refBuilding, AnagraficaSapContext db)
        {
            InitializeComponent();
            DB = db;
            RefBuilding = refBuilding;
            anagrafica = new AnagraficaSapOptions();
            buildingValueLabel.Text = RefBuilding.CodiceEdfAssociato;
            costaValueLabel.Text = RefBuilding.CodiceCdc;
            areaValueLabel.Text = $"{RefBuilding.SuperficeAttribuitaCdcCodiceSap}";
        }

        public RefBuilding RefBuilding { get; }
        public AnagraficaSapOptions anagrafica { get; set; }
        private void SapUpdateForm_Load(object sender, EventArgs e)
        {
            AddSapBuildings();

            sapBuildingGridView.Refresh();
        }

    
        private void AddSapBuildings()
        {
            foreach (var building in RefBuilding.SapBuildings)
            {
                var row = new DataGridViewRow();
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.CodiceImmobile });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.Cdc });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.UnitaEconomica8Byte });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.Ul });
                row.Cells.Add(new DataGridViewTextBoxCell { Value = building.Mq });

                sapBuildingGridView.Rows.Add(row);
            }
            sapBuildingGridView.CurrentCell = sapBuildingGridView.Rows[0].Cells[4];
        }

        private void sapBuildingGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {  
            if (e.RowIndex < 0 || e.ColumnIndex < 0) return;         
            if (sapBuildingGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].ColumnIndex==sapBuildingGridView.ColumnCount-1)
            { 
               var mq = double.Parse(sapBuildingGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());     
               RefBuilding.SapBuildings[e.RowIndex].Mq = mq;
            }
        }

        private void SapUpdateForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.X && e.Control)
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            if (e.KeyCode == Keys.S && e.Control)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }


        private void CreateSapBuilding(int rowIndex)
         {
            var building = sapBuildingGridView.Rows[rowIndex].DataBoundItem;
            if (building is RefBuildingViewModel refBuildingVM)
             {
                var refBuilding = DB.RefBuildings
                                    .Include("SapBuildings")
                                    .Single(refb => refb.Id == refBuildingVM.Id);

                var sapForm = new CreateSapBuildingForm(DB, refBuilding,anagrafica);
                var result = sapForm.ShowDialog(this);
                if (result == DialogResult.OK)
                 {
                    refBuildingVM.SuperficieSap = refBuilding.SapBuildings
                                                             .Select(sap => sap.Mq)
                                                             .Sum();
                    sapBuildingGridView.InvalidateRow(rowIndex);
                 }
             }
         }


        private void btn_DettAnagrafica_Click(object sender, EventArgs e)
        {           
            var sapForm = new CreateSapBuildingForm(DB, RefBuilding,anagrafica);
            var result = sapForm.ShowDialog(this);
        }

        
    }
}
