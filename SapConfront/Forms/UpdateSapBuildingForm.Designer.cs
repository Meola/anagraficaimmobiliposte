﻿namespace SapConfront
{
    partial class UpdateSapBuildingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateSapBuildingForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.areaValueLabel = new System.Windows.Forms.Label();
            this.buildingValueLabel = new System.Windows.Forms.Label();
            this.costaValueLabel = new System.Windows.Forms.Label();
            this.areaLabel = new System.Windows.Forms.Label();
            this.costLabel = new System.Windows.Forms.Label();
            this.buildingLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sapBuildingGridView = new System.Windows.Forms.DataGridView();
            this.Edificio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CentroDiCosto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitaEconomica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitaImmobiliare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Superficie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_DettAnagrafica = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sapBuildingGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.AliceBlue;
            this.groupBox1.Controls.Add(this.areaValueLabel);
            this.groupBox1.Controls.Add(this.buildingValueLabel);
            this.groupBox1.Controls.Add(this.costaValueLabel);
            this.groupBox1.Controls.Add(this.areaLabel);
            this.groupBox1.Controls.Add(this.costLabel);
            this.groupBox1.Controls.Add(this.buildingLabel);
            this.groupBox1.Location = new System.Drawing.Point(8, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(517, 104);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Anagrafica REF";
            // 
            // areaValueLabel
            // 
            this.areaValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.areaValueLabel.ForeColor = System.Drawing.Color.Red;
            this.areaValueLabel.Location = new System.Drawing.Point(107, 78);
            this.areaValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.areaValueLabel.Name = "areaValueLabel";
            this.areaValueLabel.Size = new System.Drawing.Size(67, 13);
            this.areaValueLabel.TabIndex = 5;
            this.areaValueLabel.Text = "super";
            this.areaValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buildingValueLabel
            // 
            this.buildingValueLabel.AutoSize = true;
            this.buildingValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buildingValueLabel.ForeColor = System.Drawing.Color.Red;
            this.buildingValueLabel.Location = new System.Drawing.Point(107, 27);
            this.buildingValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.buildingValueLabel.Name = "buildingValueLabel";
            this.buildingValueLabel.Size = new System.Drawing.Size(29, 13);
            this.buildingValueLabel.TabIndex = 4;
            this.buildingValueLabel.Text = "Edif";
            // 
            // costaValueLabel
            // 
            this.costaValueLabel.AutoSize = true;
            this.costaValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.costaValueLabel.ForeColor = System.Drawing.Color.Red;
            this.costaValueLabel.Location = new System.Drawing.Point(107, 53);
            this.costaValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.costaValueLabel.Name = "costaValueLabel";
            this.costaValueLabel.Size = new System.Drawing.Size(36, 13);
            this.costaValueLabel.TabIndex = 3;
            this.costaValueLabel.Text = "centr";
            // 
            // areaLabel
            // 
            this.areaLabel.AutoSize = true;
            this.areaLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.areaLabel.ForeColor = System.Drawing.Color.Navy;
            this.areaLabel.Location = new System.Drawing.Point(5, 77);
            this.areaLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.areaLabel.Name = "areaLabel";
            this.areaLabel.Size = new System.Drawing.Size(65, 16);
            this.areaLabel.TabIndex = 2;
            this.areaLabel.Text = "Superficie";
            // 
            // costLabel
            // 
            this.costLabel.AutoSize = true;
            this.costLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.costLabel.ForeColor = System.Drawing.Color.Navy;
            this.costLabel.Location = new System.Drawing.Point(5, 51);
            this.costLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.costLabel.Name = "costLabel";
            this.costLabel.Size = new System.Drawing.Size(98, 16);
            this.costLabel.TabIndex = 1;
            this.costLabel.Text = "Centro di Costo";
            // 
            // buildingLabel
            // 
            this.buildingLabel.AutoSize = true;
            this.buildingLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buildingLabel.ForeColor = System.Drawing.Color.Navy;
            this.buildingLabel.Location = new System.Drawing.Point(5, 25);
            this.buildingLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.buildingLabel.Name = "buildingLabel";
            this.buildingLabel.Size = new System.Drawing.Size(50, 16);
            this.buildingLabel.TabIndex = 0;
            this.buildingLabel.Text = "Edificio";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.sapBuildingGridView);
            this.groupBox2.Location = new System.Drawing.Point(8, 156);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(517, 197);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Anagrafica SAP";
            // 
            // sapBuildingGridView
            // 
            this.sapBuildingGridView.AllowUserToAddRows = false;
            this.sapBuildingGridView.AllowUserToDeleteRows = false;
            this.sapBuildingGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sapBuildingGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Edificio,
            this.CentroDiCosto,
            this.UnitaEconomica,
            this.UnitaImmobiliare,
            this.Superficie});
            this.sapBuildingGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sapBuildingGridView.Location = new System.Drawing.Point(2, 15);
            this.sapBuildingGridView.Margin = new System.Windows.Forms.Padding(2);
            this.sapBuildingGridView.Name = "sapBuildingGridView";
            this.sapBuildingGridView.RowHeadersVisible = false;
            this.sapBuildingGridView.RowTemplate.Height = 28;
            this.sapBuildingGridView.Size = new System.Drawing.Size(513, 180);
            this.sapBuildingGridView.TabIndex = 0;
            this.sapBuildingGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.sapBuildingGridView_CellEndEdit);
            this.sapBuildingGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SapUpdateForm_KeyDown);
            // 
            // Edificio
            // 
            this.Edificio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Edificio.HeaderText = "Edificio";
            this.Edificio.Name = "Edificio";
            this.Edificio.ReadOnly = true;
            // 
            // CentroDiCosto
            // 
            this.CentroDiCosto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CentroDiCosto.HeaderText = "Centro di Costo";
            this.CentroDiCosto.Name = "CentroDiCosto";
            this.CentroDiCosto.ReadOnly = true;
            // 
            // UnitaEconomica
            // 
            this.UnitaEconomica.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UnitaEconomica.HeaderText = "Unità Economica";
            this.UnitaEconomica.Name = "UnitaEconomica";
            this.UnitaEconomica.ReadOnly = true;
            // 
            // UnitaImmobiliare
            // 
            this.UnitaImmobiliare.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UnitaImmobiliare.HeaderText = "Unità Immobiliare";
            this.UnitaImmobiliare.Name = "UnitaImmobiliare";
            this.UnitaImmobiliare.ReadOnly = true;
            // 
            // Superficie
            // 
            this.Superficie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Superficie.HeaderText = "Superficie";
            this.Superficie.Name = "Superficie";
            // 
            // btn_DettAnagrafica
            // 
            this.btn_DettAnagrafica.BackColor = System.Drawing.Color.Azure;
            this.btn_DettAnagrafica.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_DettAnagrafica.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DettAnagrafica.Location = new System.Drawing.Point(421, 118);
            this.btn_DettAnagrafica.Name = "btn_DettAnagrafica";
            this.btn_DettAnagrafica.Size = new System.Drawing.Size(102, 40);
            this.btn_DettAnagrafica.TabIndex = 2;
            this.btn_DettAnagrafica.Text = "Aggiungi Anagrafica";
            this.btn_DettAnagrafica.UseVisualStyleBackColor = false;
            this.btn_DettAnagrafica.Click += new System.EventHandler(this.btn_DettAnagrafica_Click);
            // 
            // UpdateSapBuildingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 361);
            this.Controls.Add(this.btn_DettAnagrafica);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateSapBuildingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aggiornamento Anagrafica SAP";
            this.Load += new System.EventHandler(this.SapUpdateForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SapUpdateForm_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sapBuildingGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView sapBuildingGridView;
        private System.Windows.Forms.Label costaValueLabel;
        private System.Windows.Forms.Label areaLabel;
        private System.Windows.Forms.Label costLabel;
        private System.Windows.Forms.Label buildingLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Edificio;
        private System.Windows.Forms.DataGridViewTextBoxColumn CentroDiCosto;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitaEconomica;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitaImmobiliare;
        private System.Windows.Forms.DataGridViewTextBoxColumn Superficie;
        private System.Windows.Forms.Label buildingValueLabel;
        private System.Windows.Forms.Label areaValueLabel;
        private System.Windows.Forms.Button btn_DettAnagrafica;
    }
}