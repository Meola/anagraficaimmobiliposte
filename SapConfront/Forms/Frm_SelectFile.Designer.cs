﻿namespace SapConfront
{
    partial class Frm_SelectFile
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_SelectFile));
            this.refFileBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sapFileBtn = new System.Windows.Forms.Button();
            this.Lbl_Ref = new System.Windows.Forms.Label();
            this.Lbl_Sap = new System.Windows.Forms.Label();
            this.compareBuildingsBtn = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.importBuildingBtn = new System.Windows.Forms.Button();
            this.importBuildingPBar = new System.Windows.Forms.ProgressBar();
            this.importBuildingWorker = new System.ComponentModel.BackgroundWorker();
            this.statusLabel = new System.Windows.Forms.Label();
            this.archivedDataGroupBox = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelFileSap = new System.Windows.Forms.Label();
            this.labelValFileSap = new System.Windows.Forms.Label();
            this.labelFileRef = new System.Windows.Forms.Label();
            this.labelValFileRef = new System.Windows.Forms.Label();
            this.labelValDataConfronto = new System.Windows.Forms.Label();
            this.labelValDaCreare = new System.Windows.Forms.Label();
            this.labelValDaEliminare = new System.Windows.Forms.Label();
            this.labelvalDaAggiornare = new System.Windows.Forms.Label();
            this.labelValCongruente = new System.Windows.Forms.Label();
            this.DaCreare = new System.Windows.Forms.Label();
            this.DaEliminare = new System.Windows.Forms.Label();
            this.DaAggiornare = new System.Windows.Forms.Label();
            this.Congruenti = new System.Windows.Forms.Label();
            this.cancelArchiveBtn = new System.Windows.Forms.Button();
            this.FileRegButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Lbl_Regola = new System.Windows.Forms.Label();
            this.importRegolaBtn = new System.Windows.Forms.Button();
            this.Lbl_Stato = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fileStatoEdfBtn = new System.Windows.Forms.Button();
            this.Lbl_StatoUe = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fileStatoUeBtn = new System.Windows.Forms.Button();
            this.panelRegole = new System.Windows.Forms.Panel();
            this.panelRefSap = new System.Windows.Forms.Panel();
            this.importRegoleWorker = new System.ComponentModel.BackgroundWorker();
            this.archivedDataGroupBox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelRegole.SuspendLayout();
            this.panelRefSap.SuspendLayout();
            this.SuspendLayout();
            // 
            // refFileBtn
            // 
            this.refFileBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refFileBtn.Location = new System.Drawing.Point(389, 12);
            this.refFileBtn.Name = "refFileBtn";
            this.refFileBtn.Size = new System.Drawing.Size(45, 23);
            this.refFileBtn.TabIndex = 1;
            this.refFileBtn.Text = "...";
            this.refFileBtn.UseVisualStyleBackColor = true;
            this.refFileBtn.Click += new System.EventHandler(this.refFileBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "File RefBuilding: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(3, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "File SapRe: ";
            // 
            // sapFileBtn
            // 
            this.sapFileBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sapFileBtn.Location = new System.Drawing.Point(389, 38);
            this.sapFileBtn.Name = "sapFileBtn";
            this.sapFileBtn.Size = new System.Drawing.Size(45, 23);
            this.sapFileBtn.TabIndex = 4;
            this.sapFileBtn.Text = "...";
            this.sapFileBtn.UseVisualStyleBackColor = true;
            this.sapFileBtn.Click += new System.EventHandler(this.sapFileBtn_Click);
            // 
            // Lbl_Ref
            // 
            this.Lbl_Ref.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Ref.ForeColor = System.Drawing.Color.Red;
            this.Lbl_Ref.Location = new System.Drawing.Point(110, 16);
            this.Lbl_Ref.Name = "Lbl_Ref";
            this.Lbl_Ref.Size = new System.Drawing.Size(245, 18);
            this.Lbl_Ref.TabIndex = 6;
            // 
            // Lbl_Sap
            // 
            this.Lbl_Sap.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Sap.ForeColor = System.Drawing.Color.Red;
            this.Lbl_Sap.Location = new System.Drawing.Point(109, 44);
            this.Lbl_Sap.Name = "Lbl_Sap";
            this.Lbl_Sap.Size = new System.Drawing.Size(245, 18);
            this.Lbl_Sap.TabIndex = 7;
            // 
            // compareBuildingsBtn
            // 
            this.compareBuildingsBtn.BackColor = System.Drawing.Color.PaleGreen;
            this.compareBuildingsBtn.Enabled = false;
            this.compareBuildingsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.compareBuildingsBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compareBuildingsBtn.Location = new System.Drawing.Point(779, 303);
            this.compareBuildingsBtn.Name = "compareBuildingsBtn";
            this.compareBuildingsBtn.Size = new System.Drawing.Size(100, 62);
            this.compareBuildingsBtn.TabIndex = 8;
            this.compareBuildingsBtn.Text = "Confronta";
            this.compareBuildingsBtn.UseVisualStyleBackColor = false;
            this.compareBuildingsBtn.Click += new System.EventHandler(this.compareBuildingBtn_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // importBuildingBtn
            // 
            this.importBuildingBtn.BackColor = System.Drawing.Color.Azure;
            this.importBuildingBtn.Enabled = false;
            this.importBuildingBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.importBuildingBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importBuildingBtn.Location = new System.Drawing.Point(352, 98);
            this.importBuildingBtn.Name = "importBuildingBtn";
            this.importBuildingBtn.Size = new System.Drawing.Size(83, 43);
            this.importBuildingBtn.TabIndex = 10;
            this.importBuildingBtn.Text = "Importa";
            this.importBuildingBtn.UseVisualStyleBackColor = false;
            this.importBuildingBtn.Click += new System.EventHandler(this.importBuildingBtn_Click);
            // 
            // importBuildingPBar
            // 
            this.importBuildingPBar.Location = new System.Drawing.Point(5, 346);
            this.importBuildingPBar.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.importBuildingPBar.Name = "importBuildingPBar";
            this.importBuildingPBar.Size = new System.Drawing.Size(767, 18);
            this.importBuildingPBar.TabIndex = 17;
            this.importBuildingPBar.Visible = false;
            // 
            // importBuildingWorker
            // 
            this.importBuildingWorker.WorkerReportsProgress = true;
            this.importBuildingWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.importBuildingWorker_DoWork);
            this.importBuildingWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.importBuildingWorker_ProgressChanged);
            this.importBuildingWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.importBuildingWorker_RunWorkerCompleted);
            // 
            // statusLabel
            // 
            this.statusLabel.Location = new System.Drawing.Point(2, 320);
            this.statusLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(769, 23);
            this.statusLabel.TabIndex = 18;
            // 
            // archivedDataGroupBox
            // 
            this.archivedDataGroupBox.BackColor = System.Drawing.Color.Honeydew;
            this.archivedDataGroupBox.Controls.Add(this.panel1);
            this.archivedDataGroupBox.Controls.Add(this.labelValDataConfronto);
            this.archivedDataGroupBox.Controls.Add(this.labelValDaCreare);
            this.archivedDataGroupBox.Controls.Add(this.labelValDaEliminare);
            this.archivedDataGroupBox.Controls.Add(this.labelvalDaAggiornare);
            this.archivedDataGroupBox.Controls.Add(this.labelValCongruente);
            this.archivedDataGroupBox.Controls.Add(this.DaCreare);
            this.archivedDataGroupBox.Controls.Add(this.DaEliminare);
            this.archivedDataGroupBox.Controls.Add(this.DaAggiornare);
            this.archivedDataGroupBox.Controls.Add(this.Congruenti);
            this.archivedDataGroupBox.Controls.Add(this.cancelArchiveBtn);
            this.archivedDataGroupBox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.archivedDataGroupBox.Location = new System.Drawing.Point(455, 4);
            this.archivedDataGroupBox.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.archivedDataGroupBox.Name = "archivedDataGroupBox";
            this.archivedDataGroupBox.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.archivedDataGroupBox.Size = new System.Drawing.Size(426, 295);
            this.archivedDataGroupBox.TabIndex = 19;
            this.archivedDataGroupBox.TabStop = false;
            this.archivedDataGroupBox.Text = "Dati in Archivio";
            this.archivedDataGroupBox.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.labelFileSap);
            this.panel1.Controls.Add(this.labelValFileSap);
            this.panel1.Controls.Add(this.labelFileRef);
            this.panel1.Controls.Add(this.labelValFileRef);
            this.panel1.Location = new System.Drawing.Point(7, 227);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(415, 64);
            this.panel1.TabIndex = 15;
            // 
            // labelFileSap
            // 
            this.labelFileSap.AutoSize = true;
            this.labelFileSap.ForeColor = System.Drawing.Color.Navy;
            this.labelFileSap.Location = new System.Drawing.Point(3, 9);
            this.labelFileSap.Name = "labelFileSap";
            this.labelFileSap.Size = new System.Drawing.Size(60, 16);
            this.labelFileSap.TabIndex = 10;
            this.labelFileSap.Text = "File Sap:";
            // 
            // labelValFileSap
            // 
            this.labelValFileSap.AutoSize = true;
            this.labelValFileSap.ForeColor = System.Drawing.Color.Red;
            this.labelValFileSap.Location = new System.Drawing.Point(61, 9);
            this.labelValFileSap.Name = "labelValFileSap";
            this.labelValFileSap.Size = new System.Drawing.Size(0, 16);
            this.labelValFileSap.TabIndex = 14;
            // 
            // labelFileRef
            // 
            this.labelFileRef.AutoSize = true;
            this.labelFileRef.ForeColor = System.Drawing.Color.Navy;
            this.labelFileRef.Location = new System.Drawing.Point(3, 38);
            this.labelFileRef.Name = "labelFileRef";
            this.labelFileRef.Size = new System.Drawing.Size(56, 16);
            this.labelFileRef.TabIndex = 9;
            this.labelFileRef.Text = "File Ref:";
            // 
            // labelValFileRef
            // 
            this.labelValFileRef.AutoSize = true;
            this.labelValFileRef.ForeColor = System.Drawing.Color.Red;
            this.labelValFileRef.Location = new System.Drawing.Point(61, 38);
            this.labelValFileRef.Name = "labelValFileRef";
            this.labelValFileRef.Size = new System.Drawing.Size(0, 16);
            this.labelValFileRef.TabIndex = 13;
            // 
            // labelValDataConfronto
            // 
            this.labelValDataConfronto.AutoSize = true;
            this.labelValDataConfronto.Location = new System.Drawing.Point(668, 70);
            this.labelValDataConfronto.Name = "labelValDataConfronto";
            this.labelValDataConfronto.Size = new System.Drawing.Size(0, 16);
            this.labelValDataConfronto.TabIndex = 12;
            // 
            // labelValDaCreare
            // 
            this.labelValDaCreare.AutoSize = true;
            this.labelValDaCreare.ForeColor = System.Drawing.Color.Red;
            this.labelValDaCreare.Location = new System.Drawing.Point(93, 118);
            this.labelValDaCreare.Name = "labelValDaCreare";
            this.labelValDaCreare.Size = new System.Drawing.Size(58, 16);
            this.labelValDaCreare.TabIndex = 8;
            this.labelValDaCreare.Text = "dacreare";
            // 
            // labelValDaEliminare
            // 
            this.labelValDaEliminare.AutoSize = true;
            this.labelValDaEliminare.ForeColor = System.Drawing.Color.Red;
            this.labelValDaEliminare.Location = new System.Drawing.Point(93, 90);
            this.labelValDaEliminare.Name = "labelValDaEliminare";
            this.labelValDaEliminare.Size = new System.Drawing.Size(46, 16);
            this.labelValDaEliminare.TabIndex = 7;
            this.labelValDaEliminare.Text = "daelim";
            // 
            // labelvalDaAggiornare
            // 
            this.labelvalDaAggiornare.AutoSize = true;
            this.labelvalDaAggiornare.ForeColor = System.Drawing.Color.Red;
            this.labelvalDaAggiornare.Location = new System.Drawing.Point(93, 61);
            this.labelvalDaAggiornare.Name = "labelvalDaAggiornare";
            this.labelvalDaAggiornare.Size = new System.Drawing.Size(57, 16);
            this.labelvalDaAggiornare.TabIndex = 6;
            this.labelvalDaAggiornare.Text = "daaggior";
            // 
            // labelValCongruente
            // 
            this.labelValCongruente.AutoSize = true;
            this.labelValCongruente.ForeColor = System.Drawing.Color.Red;
            this.labelValCongruente.Location = new System.Drawing.Point(93, 32);
            this.labelValCongruente.Name = "labelValCongruente";
            this.labelValCongruente.Size = new System.Drawing.Size(54, 16);
            this.labelValCongruente.TabIndex = 5;
            this.labelValCongruente.Text = "conggru";
            // 
            // DaCreare
            // 
            this.DaCreare.AutoSize = true;
            this.DaCreare.ForeColor = System.Drawing.Color.Navy;
            this.DaCreare.Location = new System.Drawing.Point(4, 118);
            this.DaCreare.Name = "DaCreare";
            this.DaCreare.Size = new System.Drawing.Size(66, 16);
            this.DaCreare.TabIndex = 4;
            this.DaCreare.Text = "Da Creare";
            // 
            // DaEliminare
            // 
            this.DaEliminare.AutoSize = true;
            this.DaEliminare.ForeColor = System.Drawing.Color.Navy;
            this.DaEliminare.Location = new System.Drawing.Point(4, 90);
            this.DaEliminare.Name = "DaEliminare";
            this.DaEliminare.Size = new System.Drawing.Size(82, 16);
            this.DaEliminare.TabIndex = 3;
            this.DaEliminare.Text = "Da Eliminare";
            // 
            // DaAggiornare
            // 
            this.DaAggiornare.AutoSize = true;
            this.DaAggiornare.ForeColor = System.Drawing.Color.Navy;
            this.DaAggiornare.Location = new System.Drawing.Point(4, 61);
            this.DaAggiornare.Name = "DaAggiornare";
            this.DaAggiornare.Size = new System.Drawing.Size(89, 16);
            this.DaAggiornare.TabIndex = 2;
            this.DaAggiornare.Text = "Da Aggiornare";
            // 
            // Congruenti
            // 
            this.Congruenti.AutoSize = true;
            this.Congruenti.ForeColor = System.Drawing.Color.Navy;
            this.Congruenti.Location = new System.Drawing.Point(4, 32);
            this.Congruenti.Name = "Congruenti";
            this.Congruenti.Size = new System.Drawing.Size(70, 16);
            this.Congruenti.TabIndex = 1;
            this.Congruenti.Text = "Congruenti";
            // 
            // cancelArchiveBtn
            // 
            this.cancelArchiveBtn.BackColor = System.Drawing.Color.Red;
            this.cancelArchiveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelArchiveBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelArchiveBtn.ForeColor = System.Drawing.Color.Black;
            this.cancelArchiveBtn.Location = new System.Drawing.Point(324, 55);
            this.cancelArchiveBtn.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cancelArchiveBtn.Name = "cancelArchiveBtn";
            this.cancelArchiveBtn.Size = new System.Drawing.Size(100, 62);
            this.cancelArchiveBtn.TabIndex = 0;
            this.cancelArchiveBtn.Text = "Cancella";
            this.cancelArchiveBtn.UseVisualStyleBackColor = false;
            this.cancelArchiveBtn.Click += new System.EventHandler(this.cancelArchiveBtn_Click);
            // 
            // FileRegButton
            // 
            this.FileRegButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileRegButton.Location = new System.Drawing.Point(389, 8);
            this.FileRegButton.Name = "FileRegButton";
            this.FileRegButton.Size = new System.Drawing.Size(45, 23);
            this.FileRegButton.TabIndex = 16;
            this.FileRegButton.Text = "...";
            this.FileRegButton.UseVisualStyleBackColor = true;
            this.FileRegButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(3, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 16);
            this.label3.TabIndex = 20;
            this.label3.Text = "File Regola: ";
            // 
            // Lbl_Regola
            // 
            this.Lbl_Regola.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Regola.ForeColor = System.Drawing.Color.Red;
            this.Lbl_Regola.Location = new System.Drawing.Point(110, 13);
            this.Lbl_Regola.Name = "Lbl_Regola";
            this.Lbl_Regola.Size = new System.Drawing.Size(256, 18);
            this.Lbl_Regola.TabIndex = 21;
            // 
            // importRegolaBtn
            // 
            this.importRegolaBtn.BackColor = System.Drawing.Color.Azure;
            this.importRegolaBtn.Enabled = false;
            this.importRegolaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.importRegolaBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importRegolaBtn.Location = new System.Drawing.Point(352, 97);
            this.importRegolaBtn.Name = "importRegolaBtn";
            this.importRegolaBtn.Size = new System.Drawing.Size(83, 44);
            this.importRegolaBtn.TabIndex = 22;
            this.importRegolaBtn.Text = "Importa";
            this.importRegolaBtn.UseVisualStyleBackColor = false;
            this.importRegolaBtn.Click += new System.EventHandler(this.importRegoleBtn_Click);
            // 
            // Lbl_Stato
            // 
            this.Lbl_Stato.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Stato.ForeColor = System.Drawing.Color.Red;
            this.Lbl_Stato.Location = new System.Drawing.Point(109, 38);
            this.Lbl_Stato.Name = "Lbl_Stato";
            this.Lbl_Stato.Size = new System.Drawing.Size(257, 18);
            this.Lbl_Stato.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(2, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 16);
            this.label5.TabIndex = 24;
            this.label5.Text = "File Stato Edf: ";
            // 
            // fileStatoEdfBtn
            // 
            this.fileStatoEdfBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileStatoEdfBtn.Location = new System.Drawing.Point(389, 33);
            this.fileStatoEdfBtn.Name = "fileStatoEdfBtn";
            this.fileStatoEdfBtn.Size = new System.Drawing.Size(45, 23);
            this.fileStatoEdfBtn.TabIndex = 23;
            this.fileStatoEdfBtn.Text = "...";
            this.fileStatoEdfBtn.UseVisualStyleBackColor = true;
            this.fileStatoEdfBtn.Click += new System.EventHandler(this.FileStatoButton_Click);
            // 
            // Lbl_StatoUe
            // 
            this.Lbl_StatoUe.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_StatoUe.ForeColor = System.Drawing.Color.Red;
            this.Lbl_StatoUe.Location = new System.Drawing.Point(110, 65);
            this.Lbl_StatoUe.Name = "Lbl_StatoUe";
            this.Lbl_StatoUe.Size = new System.Drawing.Size(256, 18);
            this.Lbl_StatoUe.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(3, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 16);
            this.label6.TabIndex = 27;
            this.label6.Text = "File Stato Ue: ";
            // 
            // fileStatoUeBtn
            // 
            this.fileStatoUeBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileStatoUeBtn.Location = new System.Drawing.Point(389, 60);
            this.fileStatoUeBtn.Name = "fileStatoUeBtn";
            this.fileStatoUeBtn.Size = new System.Drawing.Size(45, 23);
            this.fileStatoUeBtn.TabIndex = 26;
            this.fileStatoUeBtn.Text = "...";
            this.fileStatoUeBtn.UseVisualStyleBackColor = true;
            this.fileStatoUeBtn.Click += new System.EventHandler(this.FileStatoUeBtn_Click);
            // 
            // panelRegole
            // 
            this.panelRegole.BackColor = System.Drawing.Color.Khaki;
            this.panelRegole.Controls.Add(this.label3);
            this.panelRegole.Controls.Add(this.importRegolaBtn);
            this.panelRegole.Controls.Add(this.Lbl_StatoUe);
            this.panelRegole.Controls.Add(this.FileRegButton);
            this.panelRegole.Controls.Add(this.label6);
            this.panelRegole.Controls.Add(this.Lbl_Regola);
            this.panelRegole.Controls.Add(this.fileStatoUeBtn);
            this.panelRegole.Controls.Add(this.fileStatoEdfBtn);
            this.panelRegole.Controls.Add(this.Lbl_Stato);
            this.panelRegole.Controls.Add(this.label5);
            this.panelRegole.Location = new System.Drawing.Point(8, 155);
            this.panelRegole.Name = "panelRegole";
            this.panelRegole.Size = new System.Drawing.Size(437, 144);
            this.panelRegole.TabIndex = 29;
            // 
            // panelRefSap
            // 
            this.panelRefSap.BackColor = System.Drawing.Color.Lavender;
            this.panelRefSap.Controls.Add(this.label1);
            this.panelRefSap.Controls.Add(this.refFileBtn);
            this.panelRefSap.Controls.Add(this.sapFileBtn);
            this.panelRefSap.Controls.Add(this.label2);
            this.panelRefSap.Controls.Add(this.Lbl_Ref);
            this.panelRefSap.Controls.Add(this.importBuildingBtn);
            this.panelRefSap.Controls.Add(this.Lbl_Sap);
            this.panelRefSap.Location = new System.Drawing.Point(8, 4);
            this.panelRefSap.Name = "panelRefSap";
            this.panelRefSap.Size = new System.Drawing.Size(437, 144);
            this.panelRefSap.TabIndex = 30;
            // 
            // importRegoleWorker
            // 
            this.importRegoleWorker.WorkerReportsProgress = true;
            this.importRegoleWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.importRegoleWorker_DoWork);
            this.importRegoleWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.importRegoleWorker_ProgressChanged);
            this.importRegoleWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.importRegoleWorker_RunWorkerCompleted);
            // 
            // Frm_SelectFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 368);
            this.Controls.Add(this.panelRefSap);
            this.Controls.Add(this.panelRegole);
            this.Controls.Add(this.archivedDataGroupBox);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.importBuildingPBar);
            this.Controls.Add(this.compareBuildingsBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_SelectFile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AnagraficaImmobile-Seleziona File";
            this.Load += new System.EventHandler(this.Frm_SelectFile_Load);
            this.archivedDataGroupBox.ResumeLayout(false);
            this.archivedDataGroupBox.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelRegole.ResumeLayout(false);
            this.panelRegole.PerformLayout();
            this.panelRefSap.ResumeLayout(false);
            this.panelRefSap.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button refFileBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button sapFileBtn;
        private System.Windows.Forms.Label Lbl_Ref;
        private System.Windows.Forms.Label Lbl_Sap;
        private System.Windows.Forms.Button compareBuildingsBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button importBuildingBtn;
        private System.Windows.Forms.ProgressBar importBuildingPBar;
        private System.ComponentModel.BackgroundWorker importBuildingWorker;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.GroupBox archivedDataGroupBox;
        private System.Windows.Forms.Button cancelArchiveBtn;
        private System.Windows.Forms.Label DaCreare;
        private System.Windows.Forms.Label DaEliminare;
        private System.Windows.Forms.Label DaAggiornare;
        private System.Windows.Forms.Label Congruenti;
        private System.Windows.Forms.Label labelValCongruente;
        private System.Windows.Forms.Label labelValDaCreare;
        private System.Windows.Forms.Label labelValDaEliminare;
        private System.Windows.Forms.Label labelvalDaAggiornare;
        private System.Windows.Forms.Label labelFileSap;
        private System.Windows.Forms.Label labelFileRef;
        private System.Windows.Forms.Label labelValDataConfronto;
        private System.Windows.Forms.Label labelValFileSap;
        private System.Windows.Forms.Label labelValFileRef;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button FileRegButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Lbl_Regola;
        private System.Windows.Forms.Button importRegolaBtn;
        private System.Windows.Forms.Label Lbl_Stato;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button fileStatoEdfBtn;
        private System.Windows.Forms.Label Lbl_StatoUe;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button fileStatoUeBtn;
        private System.Windows.Forms.Panel panelRegole;
        private System.Windows.Forms.Panel panelRefSap;
        private System.ComponentModel.BackgroundWorker importRegoleWorker;
    }
}

