﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SapConfront
{
    static class ExcelEntityMapping
    {
        public static readonly Dictionary<string, PropertyInfo> SapMapping = new Dictionary<string, PropertyInfo>
        {
            { "[codiceimmobile]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.CodiceImmobile)).Single()},
            { "[n_ord]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.n_Ord)).Single()},
            { "[struttura_ger]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.StrutturaGer)).Single()},
            { "[denomin_sege]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.DENOMINSEGE)).Single()},
            { "[denomin_cdc]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.DENOMINCDC)).Single()},
            { "[tipo_mq]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.TipoMq)).Single()},
            { "[cdc]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.Cdc)).Single()},
            { "[periodo_scarico]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.PeriodoScarico)).Single()},
            { "[mq]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.Mq)).Single()},
            { "[seco_unl_-_4byte]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.secoUNL4byte)).Single()},
            { "[tipo_d_uso_-_2byte]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.TipoDuso2Byte)).Single()},
            { "[cod_driver_-_10byte]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.CodDriver10Byte)).Single()},
            { "[unità_economica_-_8byte]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.UnitaEconomica8Byte)).Single()},
            { "[codice batch]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.CodiceBatch)).Single()},
            { "[dir/div]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.DirDiv)).Single()},
            { "[rapporto edf / strutture]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.RapportoEdfStrutture)).Single()},
            { "frazionario", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.Frazionario)).Single()},
            { "[periodo del controllo immobile]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.PeriodoControlloImmobile)).Single()},
            { "[quadrature/terreno/area]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.QuadratureTerrenoArea)).Single()},
            { "[ul]", typeof(SapBuilding).GetProperties().Where(p => p.Name == nameof(SapBuilding.Ul)).Single()},

        };


     

    }
}
