﻿namespace SapConfront
{
    partial class FrmOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOptions));
            this.ignoreUlList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.removeUlBtn = new System.Windows.Forms.Button();
            this.addUlBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textULIgnore = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboanno = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.combomese = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.saveOptionsBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ignoreUlList
            // 
            this.ignoreUlList.FormattingEnabled = true;
            this.ignoreUlList.ItemHeight = 23;
            this.ignoreUlList.Location = new System.Drawing.Point(8, 79);
            this.ignoreUlList.Margin = new System.Windows.Forms.Padding(4);
            this.ignoreUlList.Name = "ignoreUlList";
            this.ignoreUlList.Size = new System.Drawing.Size(433, 188);
            this.ignoreUlList.TabIndex = 0;
            this.ignoreUlList.DoubleClick += new System.EventHandler(this.listUL_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.AliceBlue;
            this.groupBox1.Controls.Add(this.removeUlBtn);
            this.groupBox1.Controls.Add(this.addUlBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textULIgnore);
            this.groupBox1.Controls.Add(this.ignoreUlList);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(457, 304);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edifici da Ignorare in Zre13";
            // 
            // removeUlBtn
            // 
            this.removeUlBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("removeUlBtn.BackgroundImage")));
            this.removeUlBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.removeUlBtn.Location = new System.Drawing.Point(384, 25);
            this.removeUlBtn.Margin = new System.Windows.Forms.Padding(4);
            this.removeUlBtn.Name = "removeUlBtn";
            this.removeUlBtn.Size = new System.Drawing.Size(59, 43);
            this.removeUlBtn.TabIndex = 4;
            this.removeUlBtn.UseVisualStyleBackColor = true;
            this.removeUlBtn.Click += new System.EventHandler(this.removeUlBtn_Click);
            // 
            // addUlBtn
            // 
            this.addUlBtn.BackColor = System.Drawing.Color.White;
            this.addUlBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("addUlBtn.BackgroundImage")));
            this.addUlBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addUlBtn.Location = new System.Drawing.Point(317, 25);
            this.addUlBtn.Margin = new System.Windows.Forms.Padding(4);
            this.addUlBtn.Name = "addUlBtn";
            this.addUlBtn.Size = new System.Drawing.Size(59, 43);
            this.addUlBtn.TabIndex = 3;
            this.addUlBtn.UseVisualStyleBackColor = false;
            this.addUlBtn.Click += new System.EventHandler(this.addUlBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "UL da Ignorare";
            // 
            // textULIgnore
            // 
            this.textULIgnore.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textULIgnore.Location = new System.Drawing.Point(139, 31);
            this.textULIgnore.Margin = new System.Windows.Forms.Padding(4);
            this.textULIgnore.MaxLength = 8;
            this.textULIgnore.Name = "textULIgnore";
            this.textULIgnore.Size = new System.Drawing.Size(169, 30);
            this.textULIgnore.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.AliceBlue;
            this.groupBox2.Controls.Add(this.comboanno);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.combomese);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(500, 9);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(475, 304);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Periodo di Scarico";
            // 
            // comboanno
            // 
            this.comboanno.FormattingEnabled = true;
            this.comboanno.Items.AddRange(new object[] {
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022"});
            this.comboanno.Location = new System.Drawing.Point(197, 118);
            this.comboanno.Margin = new System.Windows.Forms.Padding(4);
            this.comboanno.MaxLength = 4;
            this.comboanno.Name = "comboanno";
            this.comboanno.Size = new System.Drawing.Size(271, 31);
            this.comboanno.TabIndex = 5;
            this.comboanno.Validated += new System.EventHandler(this.comboanno_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 122);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Periodo Scarico Anno";
            // 
            // combomese
            // 
            this.combomese.FormattingEnabled = true;
            this.combomese.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.combomese.Location = new System.Drawing.Point(197, 60);
            this.combomese.Margin = new System.Windows.Forms.Padding(4);
            this.combomese.MaxLength = 2;
            this.combomese.Name = "combomese";
            this.combomese.Size = new System.Drawing.Size(271, 31);
            this.combomese.TabIndex = 3;
            this.combomese.Validated += new System.EventHandler(this.combomese_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Periodo Scarico Mese";
            // 
            // saveOptionsBtn
            // 
            this.saveOptionsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.saveOptionsBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveOptionsBtn.Location = new System.Drawing.Point(697, 322);
            this.saveOptionsBtn.Margin = new System.Windows.Forms.Padding(4);
            this.saveOptionsBtn.Name = "saveOptionsBtn";
            this.saveOptionsBtn.Size = new System.Drawing.Size(132, 68);
            this.saveOptionsBtn.TabIndex = 4;
            this.saveOptionsBtn.Text = "Salva";
            this.saveOptionsBtn.UseVisualStyleBackColor = false;
            this.saveOptionsBtn.Click += new System.EventHandler(this.saveOptionsBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.BackColor = System.Drawing.Color.Silver;
            this.cancelBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBtn.Location = new System.Drawing.Point(843, 322);
            this.cancelBtn.Margin = new System.Windows.Forms.Padding(4);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(132, 68);
            this.cancelBtn.TabIndex = 5;
            this.cancelBtn.Text = "Chiudi";
            this.cancelBtn.UseVisualStyleBackColor = false;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // FrmOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(981, 394);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.saveOptionsBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmOptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AnagraficaImmobiliare - Opzioni";
            this.Load += new System.EventHandler(this.FrmOptions_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmOptions_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox ignoreUlList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textULIgnore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addUlBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboanno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox combomese;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button saveOptionsBtn;
        private System.Windows.Forms.Button removeUlBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}