﻿namespace SapConfront
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonSelezionafile = new System.Windows.Forms.ToolStripButton();
            this.toolStripSalvatutto = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOpzioni = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExit = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.PowderBlue;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(50, 50);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonSelezionafile,
            this.toolStripSalvatutto,
            this.toolStripButtonOpzioni,
            this.toolStripButtonExit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(913, 75);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonSelezionafile
            // 
            this.toolStripButtonSelezionafile.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonSelezionafile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSelezionafile.Image")));
            this.toolStripButtonSelezionafile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSelezionafile.Name = "toolStripButtonSelezionafile";
            this.toolStripButtonSelezionafile.Size = new System.Drawing.Size(111, 72);
            this.toolStripButtonSelezionafile.Text = "Seleziona File";
            this.toolStripButtonSelezionafile.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonSelezionafile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonSelezionafile.Click += new System.EventHandler(this.toolStripButtonSelezionafile_Click);
            // 
            // toolStripSalvatutto
            // 
            this.toolStripSalvatutto.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripSalvatutto.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSalvatutto.Image")));
            this.toolStripSalvatutto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSalvatutto.Name = "toolStripSalvatutto";
            this.toolStripSalvatutto.Size = new System.Drawing.Size(88, 72);
            this.toolStripSalvatutto.Text = "Salva Tutto";
            this.toolStripSalvatutto.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripSalvatutto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripSalvatutto.Click += new System.EventHandler(this.toolstripSalvatutto_Click);
            // 
            // toolStripButtonOpzioni
            // 
            this.toolStripButtonOpzioni.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonOpzioni.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpzioni.Image")));
            this.toolStripButtonOpzioni.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpzioni.Name = "toolStripButtonOpzioni";
            this.toolStripButtonOpzioni.Size = new System.Drawing.Size(65, 72);
            this.toolStripButtonOpzioni.Text = "Opzioni";
            this.toolStripButtonOpzioni.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonOpzioni.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonOpzioni.Click += new System.EventHandler(this.toolStripButtonOpzioni_Click);
            // 
            // toolStripButtonExit
            // 
            this.toolStripButtonExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonExit.Image")));
            this.toolStripButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExit.Name = "toolStripButtonExit";
            this.toolStripButtonExit.Size = new System.Drawing.Size(54, 72);
            this.toolStripButtonExit.Text = "Esci";
            this.toolStripButtonExit.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButtonExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonExit.Click += new System.EventHandler(this.toolStripButtonExit_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(913, 487);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AnagraficaImmobile";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonSelezionafile;
        private System.Windows.Forms.ToolStripButton toolStripButtonExit;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpzioni;
        private System.Windows.Forms.ToolStripButton toolStripSalvatutto;
    }
}