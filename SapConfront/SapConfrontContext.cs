﻿namespace SapConfront
{
    using System.Collections.Generic;
    using System.Data.Entity;

    public partial class RefBuilding
    {
        public int Id { get; set; }

        public string CodiceImmobile { get; set; }
        public string CodiceEdfAssociato { get; set; }
        public string TipoImmobile { get; set; }
        public string StatoGestionale { get; set; }
        public string UtilizzoPrevalente { get; set; }
        public string DenominazioneImmobile { get; set; }
        public string AreaImmobiliare { get; set; }
        public string Regione { get; set; }
        public string Provincia { get; set; }
        public string Comune { get; set; }
        public string CodiceCdc { get; set; }
        public string CentroDiCosto { get; set; }
        public string SuperficeAttribuita { get; set; }

        public ICollection<DatiSap> DatiSap { get; set; }
    }

    public partial class DatiSap
    {
        public int Id { get; set; }

        public string Progr { get; set; }
        public string n_Ord { get; set; }
        public string StrutturaGer { get; set; }
        public string DENOMINSEGE { get; set; }
        public string DENOMINCDC { get; set; }
        public string CodiceImmobile { get; set; }
        public string TipoMq { get; set; }
        public string Cdc { get; set; }
        public string TipoMqCalc { get; set; }
        public string PeriodoScarico { get; set; }
        public string EsisteImmobile { get; set; }
        public string Aperto { get; set; }
        public string mqProdVivi { get; set; }
        public string UlCalc1 { get; set; }
        public string UlCalc2 { get; set; }
        public string Ul { get; set; }
        public string Mq { get; set; }
        public string MqRef { get; set; }
        public string Differenza { get; set; }
        public string secoUNL4byte { get; set; }
        public string TipoDuso2Byte { get; set; }
        public string CodDriver10Byte { get; set; }
        public string UnitaEconomica8Byte { get; set; }
        public string UeUl { get; set; }
        public string CodiceBatch { get; set; }
        public string UeUlCdc { get; set; }
        public string DirDiv { get; set; }
        public string RapportoEdfStrutture { get; set; }
        public string Frazionario { get; set; }
        public string PeriodoControlloImmobile { get; set; }
        public string QuadratureTerrenoArea { get; set; }
        public string UeEdfUl { get; set; }

        public ICollection<RefBuilding> RefBuilding { get; set; }
    }


    public class SapConfrontContext : DbContext
    {
        public SapConfrontContext()
            : base("name=SapConfrontContext")
        {
        }

        public virtual DbSet<RefBuilding> RefBuildings { get; set; }
        public virtual DbSet<DatiSap> DatiSaps { get; set; }


        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<RefBuilding>()
                .HasKey(rb => rb.Id)
                .HasMany(rb => rb.DatiSap);
            builder.Entity<RefBuilding>()
                .HasIndex(rb => new { rb.CodiceCdc, rb.CodiceEdfAssociato });
        }


    }

}