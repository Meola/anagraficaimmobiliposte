﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;


namespace SapConfront
{
    public class AnagraficaSapOptions
    {
        public static AnagraficaSapOptions DEFAULT => new AnagraficaSapOptions();

        public List<string> IgnoraUlInZRE13 { get; set; } = Enumerable.Empty<string>().ToList();

        public int PeriodoScaricoMese { get; set; } = DateTime.Today.Month;

        public int PeriodoScaricoAnno { get; set; } = DateTime.Today.Year;

        public static AnagraficaSapOptions Read(AnagraficaSapContext context)
        {
            var options = context.Options
                                 .Find(nameof(AnagraficaSapOptions));
            if (options == null) return DEFAULT;
            if (options.Value == null) return DEFAULT;

            var anagraficaSapOptions = 
                JsonConvert.DeserializeObject<AnagraficaSapOptions>(options.Value);

            return anagraficaSapOptions;
        }

        public static void Save(AnagraficaSapContext context, AnagraficaSapOptions anagraficaSapOptions)
        {
            var options = context.Options
                                 .Find(nameof(AnagraficaSapOptions));
            var jsonOptions = JsonConvert.SerializeObject(anagraficaSapOptions);

            if (options == null)
            {
                context.Options.Add(new Options { Key = nameof(AnagraficaSapOptions), Value = jsonOptions });
            }
            else
            {
                options.Value = jsonOptions;
            }

            context.SaveChanges();
        }
    }
}