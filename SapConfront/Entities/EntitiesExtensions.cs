﻿using System;

namespace SapConfront
{
    public static class EntitiesExtensions
    {
        public static string GetModifySapUeUl(this SapBuilding sapBuilding,AnagraficaSapOptions anagrafica)
        {
            return $"{sapBuilding.UnitaEconomica8Byte}".PadRight(8).ToUpper() +
                $"{sapBuilding.Ul}".PadRight(8).ToUpper() +
                $"{sapBuilding.Cdc}".PadRight(35) +
                $"{sapBuilding.Mq}".PadRight(13) +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}" +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}";
        }

        public static string GetModifySapCdc(this SapBuilding sapBuilding, AnagraficaSapOptions anagrafica)
        {
            return $"        " +
                $"        " +
                $"{sapBuilding.Cdc}".PadRight(35) +
                $"{sapBuilding.Mq}".PadRight(13) +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}" +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}";
        }

        public static string GetCreateSapUeUl(this SapBuilding sapBuilding, AnagraficaSapOptions anagrafica)
        {
            return $"{sapBuilding.UnitaEconomica8Byte}".PadRight(8) +
                $"{sapBuilding.Ul}".PadRight(8) +
                $"{sapBuilding.TipoDuso2Byte}".PadRight(2) +
                $"{sapBuilding.DENOMINSEGE}".PadRight(30) +
                $"{sapBuilding.CodiceImmobile}".PadRight(8) +
                $"{sapBuilding.secoUNL4byte}".PadRight(4) +
                $"{sapBuilding.StrutturaGer}".PadRight(20) +
                $"/".PadRight(20) +
                $"{sapBuilding.CodDriver10Byte}".PadRight(10) +
                $"{sapBuilding.Cdc}".PadRight(35) +
                $"{sapBuilding.Mq}".PadRight(13) +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}" +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}" +
                $"{SapBuilding.SAP_DATE_REFERENCE}";
        }

        public static string GetCreateSapCdc(this SapBuilding sapBuilding, AnagraficaSapOptions anagrafica)
        {
            return string.Empty.PadRight(8) +
                string.Empty.PadRight(8) +
                string.Empty.PadRight(2) +
                string.Empty.PadRight(30) +
                string.Empty.PadRight(8) +
                string.Empty.PadRight(4) +
                string.Empty.PadRight(20) +
                string.Empty.PadRight(20) +
                string.Empty.PadRight(10) +
                $"{sapBuilding.Cdc}".PadRight(35) +
                $"{sapBuilding.Mq}".PadRight(13) +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}" +
                $"{sapBuilding.PeriodoScarico}".PadLeft(3, '0') +
                $"{anagrafica.PeriodoScaricoAnno}" +
                $"{SapBuilding.SAP_DATE_REFERENCE}";
        }
    }
}
