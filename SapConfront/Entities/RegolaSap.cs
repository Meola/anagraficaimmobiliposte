﻿using System.Collections.Generic;
using System.Globalization;

namespace SapConfront
{
    public partial class RegolaSap
    {
        public int Id { get; set; }
        public string Ue { get; set; }
        public string Edifici { get; set; }
        public string Numerooggetto { get; set; }
        public string Tp { get; set; }
        public string Testofabbricato { get; set; }
        public int Cfrequiv { get; set; }
        public int Damm { get; set; }
        public string Daaa { get; set; }
        public string Amm { get; set; }
        public string Aaa { get; set; }
        public string Noggdestsccosti { get; set; }
        public string  UL{ get; set; }

        public List<SapBuilding> SapBuildings { get; set; }

    }

}
