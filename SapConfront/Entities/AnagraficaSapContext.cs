﻿namespace SapConfront
{
    using System.Data.Entity;

    public class AnagraficaSapContext : DbContext
    {
        public AnagraficaSapContext(string connectionStringName)
            : base($"name={connectionStringName}") { }

        public virtual DbSet<LastActionRefSap> LastActionRefSaps { get; set; }
        public virtual DbSet<RefBuilding> RefBuildings { get; set; }
        public virtual DbSet<SapBuilding> SapBuildings { get; set; }
        public virtual DbSet<RegolaSap> RegoleSap { get; set; }
        public virtual DbSet<StatoEdf> StatiEdf { get; set; }
        public virtual DbSet<StatoUe> StatiUe { get; set; }
        public virtual DbSet<Options> Options { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<RefBuilding>()
                   .HasKey(c => c.Id)
                   .HasMany(rb => rb.SapBuildings)
                   .WithMany(sap => sap.RefBuildings)
                   .Map(sr =>
                   {
                       sr.MapLeftKey("RefBuildingId");
                       sr.MapRightKey("SapBuildingId");
                       sr.ToTable("SapBuildingsRefBuildings");
                   });
            builder.Entity<SapBuilding>()
                .HasKey(sap => sap.Id)
                .HasRequired(sap => sap.RegolaSap)
                .WithMany(r => r.SapBuildings)
                .HasForeignKey(sap => sap.RegolaSap_Id);
            builder.Entity<SapBuilding>()
                .HasRequired(sap => sap.StatoUe)
                .WithMany(r => r.SapBuildings)
                .HasForeignKey(sap => sap.StatoUe_Id);
            builder.Entity<SapBuilding>()
                .HasRequired(sap => sap.StatoEdf)
                .WithMany(r => r.SapBuildings)
                .HasForeignKey(sap => sap.StatoEdf_Id);

            builder.Entity<LastActionRefSap>()
                .HasKey(c => c.Id);

            builder.Entity<RegolaSap>()
                .HasKey(c => c.Id);

            builder.Entity<StatoEdf>()
                .HasKey(edf => edf.Id);

            builder.Entity<StatoUe>()
                .HasKey(ue => ue.Id);
        }
    }
}
