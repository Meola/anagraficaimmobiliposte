﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SapConfront
{

    public partial class RefBuilding
    {
        public int Id { get; set; }
        public string CodiceImmobile { get; set; }
        public string CodiceEdfAssociato { get; set; }
        public string AiOld { get; set; }
        public string MacroAreaNew { get; set; }
        public string TipoImmobile { get; set; }
        public string UtilizzoPrevalente { get; set; }
        public string Regione { get; set; }
        public string Provincia { get; set; }
        public string Comune { get; set; }
        public string DenominazioneImmobile { get; set; }
        public string CodiceCdc { get; set; }
        public string CentroDiCosto { get; set; }
        public double SuperficeAttribuitaCdcCodiceImmobile { get; set; }
        public double PercSuperficeCodSap { get; set; }
        public double SuperficeAttribuitaCdcCodiceSap { get; set; }

        public List<SapBuilding> SapBuildings { get; set; }

        internal static RefBuilding Aggregate(params RefBuilding[] refBuildings)
        {
            if (refBuildings == null) throw new ArgumentNullException(nameof(refBuildings));
            if (refBuildings.Length == 0) throw new ArgumentOutOfRangeException(nameof(refBuildings));

            var refBuilding = Copy(refBuildings[0]);
            refBuilding.SuperficeAttribuitaCdcCodiceSap = refBuildings.Sum(refb => refb.SuperficeAttribuitaCdcCodiceSap);

            return refBuilding;
        }

        internal static RefBuilding Copy(RefBuilding refBuilding)
        {
            return new RefBuilding
            {
                AiOld = refBuilding.AiOld,
                CentroDiCosto = refBuilding.CentroDiCosto,
                CodiceCdc = refBuilding.CodiceCdc,
                CodiceEdfAssociato = refBuilding.CodiceEdfAssociato,
                CodiceImmobile = refBuilding.CodiceImmobile,
                Comune = refBuilding.Comune,
                DenominazioneImmobile = refBuilding.DenominazioneImmobile,
                Id = refBuilding.Id,
                MacroAreaNew = refBuilding.MacroAreaNew,
                PercSuperficeCodSap = refBuilding.PercSuperficeCodSap,
                Provincia = refBuilding.Provincia,
                Regione = refBuilding.Regione,
                SapBuildings = refBuilding.SapBuildings,
                SuperficeAttribuitaCdcCodiceImmobile = refBuilding.SuperficeAttribuitaCdcCodiceImmobile,
                SuperficeAttribuitaCdcCodiceSap = refBuilding.SuperficeAttribuitaCdcCodiceSap,
                TipoImmobile = refBuilding.TipoImmobile,
                UtilizzoPrevalente = refBuilding.UtilizzoPrevalente
            };
        }
    }
}