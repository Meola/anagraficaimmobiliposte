﻿namespace SapConfront
{
    public partial class LastActionRefSap
    {
        public int Id { get; set; }
        //Ultimo Ref importato
        public string NameRefImport { get; set; }
        //Ultimo Sap importato
        public string NameSapImport { get; set; }
    }
}