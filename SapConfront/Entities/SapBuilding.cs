﻿using System.Collections.Generic;

namespace SapConfront
{
    public partial class SapBuilding
    {
        internal static readonly string SAP_DATE_REFERENCE = "01012000";

        public int Id { get; set; }
        public string n_Ord { get; set; }
        public string StrutturaGer { get; set; }
        public string DENOMINSEGE { get; set; }
        public string DENOMINCDC { get; set; }
        public string CodiceImmobile { get; set; }
        public string TipoMq { get; set; }
        public string Cdc { get; set; }
        public string PeriodoScarico { get; set; }
        public string Ul { get; set; }
        public string secoUNL4byte { get; set; }
        public double Mq { get; set; }
        public string TipoDuso2Byte { get; set; }
        public string CodDriver10Byte { get; set; }
        public string UnitaEconomica8Byte { get; set; }
        public string CodiceBatch { get; set; }
        public string DirDiv { get; set; }
        public string RapportoEdfStrutture { get; set; }
        public string Frazionario { get; set; }
        public string PeriodoControlloImmobile { get; set; }
        public string QuadratureTerrenoArea { get; set; }
        //campo con valore concatenazione ue economica 8 byte con ul
        public string UEUL { get; set; }

        public bool NuovoEdificio { get; set; }

        public List<RefBuilding> RefBuildings { get; set; }

        public RegolaSap RegolaSap { get; set; }
        public int RegolaSap_Id { get; set; }

        public StatoEdf StatoEdf { get; set; }
        public int StatoEdf_Id { get; set; }

        public StatoUe StatoUe { get; set; }
        public int StatoUe_Id { get; set; }
    }
}