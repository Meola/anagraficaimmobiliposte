﻿using System.Collections.Generic;

namespace SapConfront
{
    public partial class StatoUe
    {
        public int Id { get; set; }
        public string Unitàeconomica { get; set; }
        public string CAP { get; set; }
        public string Indirizzolocalità { get; set; }
        public string Indirizzoviancivico { get; set; }
        public string Statosettatodalsistema { get; set; }

        public string UE => Unitàeconomica.Substring(0, 8);

        public List<SapBuilding> SapBuildings { get; set; }
    }
}
