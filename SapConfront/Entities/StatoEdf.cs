﻿using System.Collections.Generic;

namespace SapConfront
{
    public partial class StatoEdf
    {

        public int Id { get; set; }
        public string Edifici1 { get; set; }
        public string Indirizzolocalità { get; set; }
        public string CAP { get; set; }
        public string Indirizzoviancivico { get; set; }
        public string Edifici2 { get; set; }
        public string UE { get; set; }
        public string Fabbricatotipo { get; set; }
        public string Statosettatodalsistema { get; set; }
        public string SeCo { get; set; }

        public List<SapBuilding> SapBuildings { get; set; }
    }
}
